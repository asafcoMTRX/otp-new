function getViewport_width() {
    var viewPortWidth;
    if (typeof window.innerWidth != 'undefined') { viewPortWidth = window.innerWidth }
    else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) { viewPortWidth = document.documentElement.clientWidth }
    else { viewPortWidth = document.getElementsByTagName('body')[0].clientWidth; }
    return viewPortWidth;
}

$.fn.exists = function () {
    return this.length !== 0;
}

function equalHeights(objClass) {
    objClass.css('height', 'auto');
    temp = 0;
    objClass.each(function (index) { if ($(this).height() > temp) { temp = $(this).height(); } });
    if (temp == 0) { setTimeout(function () { equalHeights(objClass); }, 50); }
    objClass.css('height', temp + "px");
}

/* header */
function toggleMobileMenu() {
    mobileMenu = document.getElementById('headerMenu_box');
    if ($(mobileMenu).hasClass('headerMenu_box_active')) $(mobileMenu).removeClass('headerMenu_box_active');
    else $(mobileMenu).addClass('headerMenu_box_active');
}

var megaMenu_time = [0, 0, 0, 0, 0];
function closeMegamenu(objLink, whichNum, timeNum) {
    megaMenu_time[timeNum] = setTimeout(function () {
        if (whichNum == 1) $(objLink).parent().parent().css('display', 'none');
        else if (whichNum == 2) $(objLink).parent().css('display', 'none');
        setTimeout(function () { $('.megaMenu').css('display', ''); }, 50);
    }, 50);
}

function megaMenu_keepOpen(objLink, whichNum, timeNum) {
    clearTimeout(megaMenu_time[timeNum]);
    if (whichNum == 1) $(objLink).parent().parent().css('display', 'block');
    else if (whichNum == 2) $(objLink).parent().css('display', 'block');
}

function toggleSearchBar() {
    if (getViewport_width() < 671) {
        if (!($('.headerSearch_toolbar').hasClass('headerSearch_toolbar_active'))) {
            $('.headerSearch_toolbar').addClass('headerSearch_toolbar_active');
            setTimeout('refresh_toggleSearchBar()', 50);
        }
        else {
            $('.headerSearch_toolbar').removeClass('headerSearch_toolbar_active');
            setTimeout(function () {
                $('.headerSearch_toolbar').css('top', '');
                $('.header').css('margin-bottom', '');
            }, 50);
        }
    }
    else {
        $('.headerSearch_toolbar').css('display', '');
        $('.headerSearch_toolbar').css('top', '');
        $('.header').css('margin-bottom', '');
    }
}

function refresh_toggleSearchBar() {
    if (getViewport_width() < 671) {
        if ($(".headerSearch_toolbar_active").exists()) {
            $('.headerSearch_toolbar').css('top', $('.header').outerHeight());
            $('.header').css('margin-bottom', $('.headerSearch_toolbar').outerHeight());
        }
    }
    else {
        $('.headerSearch_toolbar').removeClass('headerSearch_toolbar_active');
        $('.headerSearch_toolbar').css('top', '');
        $('.header').css('margin-bottom', '');
    }
}

$("body").click(function () {
    if (getViewport_width() <= 700) $('.headerSearch_toolbar').removeClass('headerSearch_toolbar_active');
    $('#headerMenu_box').removeClass('headerMenu_box_active');
});

$(".headerSearch").click(function (e) { e.stopPropagation(); });
$('.headerIcon').click(function (e) { e.stopPropagation(); });
$('#headerMenu_box').click(function (e) { e.stopPropagation(); });

/* matmid */
function toggleThis(obj) {
    if (!($(obj).hasClass('greyFrame_toggleActive'))) {
        $(obj).parent().find('a').removeClass('greyFrame_toggleActive');
        $(obj).addClass('greyFrame_toggleActive');
    }
}

function show_chkList(obj, bin) {
    if (bin) $(obj).parent().find('.passwordChkList').show();
    else $(obj).parent().find('.passwordChkList').hide();
}

function toggleContent(bin, obj1, obj2) {
    if (bin) {
        $("#" + obj1).show();
        $("#" + obj2).hide();
    }
    else {
        $("#" + obj1).hide();
        $("#" + obj2).show();
    }
}

function setup_donationSelection() {
    $('.clubDonation_item a').each(function (index) {
        $(this).on("click", function (e) {
            $('.clubDonation li').removeClass('clubDonation_itemActive');
            $(this).parent().addClass('clubDonation_itemActive');
            return false;
        });
    });
}

/* content */
function toggle_readMore(objLink) {
    readMore_obj = objLink.parentNode;
    if (readMore_obj.className.indexOf('readMore_open') != -1) {
        readMore_obj.className = "readMore";
        objLink.innerHTML = "קרא עוד";
    }
    else {
        readMore_obj.className = "readMore readMore_open";
        objLink.innerHTML = "סגור";
    }
}

function flightUpgrades_height() {
    if (!($('.flightUpgrades ul li a img').height() > 0)) setTimeout(function () { flightUpgrades_height() }, 50);
    $('.flightUpgrades ul li a').css('height', 'auto');
    temp = 0;
    $('.flightUpgrades ul li a').each(function (index) { if ($(this).height() > temp) temp = $(this).height(); });
    $('.flightUpgrades ul li a').css('height', temp + "px");
}

function contentFrames_heights() {
    $('.luggage_moreGuidelines li span').css('height', 'auto');
    temp = 0;
    $('.luggage_moreGuidelines li span').each(function (index) { if ($(this).height() > temp) temp = $(this).height(); });
    $('.luggage_moreGuidelines li span').css('height', temp + "px");

    flightUpgrades_height();

    if (getViewport_width() > 870) {
        $('.regionalGuidelines_td th').css('height', 'auto');
        temp = 0;
        $('.regionalGuidelines_td th').each(function (index) { if ($(this).height() > temp) temp = $(this).height(); });
        $('.regionalGuidelines_td th').css('height', temp + "px");

        $('.regionalGuidelines_td tr.regionalGuidelines_secondaryTH th').css('height', 'auto');
        temp = 0;
        $('.regionalGuidelines_td tr.regionalGuidelines_secondaryTH th').each(function (index) { if ($(this).height() > temp) temp = $(this).height(); });
        $('.regionalGuidelines_td tr.regionalGuidelines_secondaryTH th').css('height', temp + "px");

        $('.regionalGuidelines_td table tr td').css('height', 'auto');
        temp = 0;
        $('.regionalGuidelines_td table tr td').each(function (index) { if ($(this).height() > temp) temp = $(this).height(); });
        $('.regionalGuidelines_td table tr td').css('height', temp + "px");
    }
    else {
        $('.regionalGuidelines_td th').css('height', 'auto');
        $('.regionalGuidelines_td tr.regionalGuidelines_secondaryTH th').css('height', 'auto');
        $('.regionalGuidelines_td table tr td').css('height', 'auto');
    }

    yaadFrames_heights();
}

function yaadFrames_heights() {
    if (getViewport_width() > 800) {
        $('.additionalOffers_list .hotDeals_item').css('height', 'auto');
    }
    else {
        $('.additionalOffers_list .hotDeals_itemText').each(function (index) {
            temp = $(this).height();
            $(this).parent().find('.hotDeals_itemImage').css('height', temp + "px");
            $(this).parent().parent().css('height', temp + "px");
        });
    }
}

function dealTerms_toggle(objLInk) {
    termsText = document.getElementById('dealTerms_text');
    offerBtn = document.getElementById('openedDeal_offerBtn');
    if (termsText.style.display != 'block') {
        termsText.style.display = 'block';
        $('.innerContent.grey-upgrade .dealTerms_text').css("display", "block");
        if (offerBtn != null) offerBtn.style.display = 'block';
        objLInk.getElementsByTagName('B')[0].innerHTML = "-";
    }



    else {
        termsText.style.display = 'none';
        $('.innerContent.grey-upgrade .dealTerms_text').css("display", "none");
        if (offerBtn != null) offerBtn.style.display = 'none';
        objLInk.getElementsByTagName('B')[0].innerHTML = "+";
    }
}




/* flight engine */
function toggle_passengerDetails(objLink, objBin) {
    frameObj = $($('.plazmaTab_formFrame')[0]);
    frameObj.css('overflow', 'visible');
    frameObj.height('auto');

    if (objBin) {
        $('.plazmaTab_formFrame_primo_passengersDetails').css('display', 'block');
        objLink.innerHTML = "הסתר<br>סוגי נוסעים";
        objLink.onclick = function () { toggle_passengerDetails(this, false); return false; }
        //		setTimeout(function(){agenciesMobileConfig();}, 50);
    }
    else {
        $('.plazmaTab_formFrame_primo_passengersDetails').css('display', 'none');
        objLink.innerHTML = "סוגי נוסעים<br>נוספים&nbsp;&gt;";
        objLink.onclick = function () { toggle_passengerDetails(this, true); return false; }
        //		setTimeout(function(){agenciesMobileConfig();}, 50);
    }
}


/* flight engine */
function toggle_passengerDetails_GreyPage(objLink, objBin) {
    frameObj = $($('.plazmaTab_formFrame')[0]);
    frameObj.css('overflow', 'visible');
    frameObj.height('auto');


    image1 = new Image();
    image1.src = "/images/down_round_arrow.png";

    var img = document.createElement('image');
    img.src = "/images/down_round_arrow.png";
    img.width = 22;
    img.height = 22;



    if (objBin) {
        $('.plazmaTab_formFrame_primo_passengersDetails').css('display', 'block');
        objLink.innerHTML = "<span>  סוגי נוסעים <br>נוספים </span>" + "<div class='Div_plazmaTab_formFrame_primo_passengersDetails_up_round_arrow' ><img class='plazmaTab_formFrame_primo_passengersDetails_up_round_arrow' src='imgs/up_round_arrow.png' /><div>";

        objLink.onclick = function () { toggle_passengerDetails_GreyPage(this, false); return false; }
        //		setTimeout(function(){agenciesMobileConfig();}, 50);
    }
    else {
        $('.plazmaTab_formFrame_primo_passengersDetails').css('display', 'none');
        

        objLink.innerHTML = "<span>   הסתר<br> סוגי נוסעים </span>" + "<div class='Div_plazmaTab_formFrame_primo_passengersDetails_down_round_arrow' ><img class='plazmaTab_formFrame_primo_passengersDetails_down_round_arrow' src='imgs/down_round_arrow.png' /><div>";

        objLink.onclick = function () { toggle_passengerDetails_GreyPage(this, true); return false; }
        //		setTimeout(function(){agenciesMobileConfig();}, 50);
    }
}

/* flight engine */
function toggle_passengerDetails_GreyPage_en(objLink_en, objBin) {
    frameObj_en = $($('.plazmaTab_formFrame')[0]);
    frameObj_en.css('overflow', 'visible');
    frameObj_en.height('auto');

    if (objBin) {
        $('.plazmaTab_formFrame_primo_passengersDetails').css('display', 'block');
        objLink_en.innerHTML = "<span>  Hide<br> Passenger Types</span>" + "<div class='Div_plazmaTab_formFrame_primo_passengersDetails_up_round_arrow' ><img class='plazmaTab_formFrame_primo_passengersDetails_up_round_arrow' src='imgs/up_round_arrow.png' /><div>";
        objLink_en.onclick = function () { toggle_passengerDetails_GreyPage_en(this, false); return false; }
        //		setTimeout(function(){agenciesMobileConfig();}, 50);
    }
    else {
        $('.plazmaTab_formFrame_primo_passengersDetails').css('display', 'none');
        objLink_en.innerHTML = "<span>  Show  <br> Passengers Types </span>" + "<div class='Div_plazmaTab_formFrame_primo_passengersDetails_down_round_arrow' ><img class='plazmaTab_formFrame_primo_passengersDetails_down_round_arrow' src='imgs/down_round_arrow.png' /><div>";
        objLink_en.onclick = function () { toggle_passengerDetails_GreyPage_en(this, true); return false; }
        //		setTimeout(function(){agenciesMobileConfig();}, 50);
    }
}




function toggleCalendar(calPopId, anchorName) {
    if ($('#' + calPopId).css('display') != 'block') {
        $('.calendarPop').css('display', 'none');
        $('.dateDetails_box').css('z-index', '220');
        $('#' + calPopId).parent().css('z-index', '230');
        $('#' + calPopId).css('display', 'block');

        if ((getViewport_width() <= 640) && anchorName) {
            $('html, body').animate({ scrollTop: $('#' + anchorName).offset().top - 10 }, 0);
        }
    }
    else {
        $('#' + calPopId).css('display', 'none');
        $('.dateDetails_box').css('z-index', '220');
    }
}

var hebDays = ['ראשון', 'שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת'];
var hebMonths = ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'];

function initCalendar(dateID) {
    dateIDX = document.getElementById(dateID);
    var todayx = new Date;
    dayx = todayx.getDate();
    monx = todayx.getMonth() + 1;
    yearx = todayx.getFullYear();
    dateText = dayx + "/" + monx + "/" + yearx;
    calPop = $("#calendarPop_from" + (dateIDX.id.substr(dateIDX.id.length - 2)));
    //	setupCalendar(calPop, dateText);

    var nextWeek = new Date(todayx.getTime() + (7 * 24 * 60 * 60 * 1000));
    dayx = nextWeek.getDate();
    monx = nextWeek.getMonth() + 1;
    yearx = nextWeek.getFullYear();
    dateText = dayx + "/" + monx + "/" + yearx;
    calPop = $("#calendarPop_to" + (dateIDX.id.substr(dateIDX.id.length - 2)));
    //	setupCalendar(calPop, dateText);
}

function setupCalendar(calPop_obj, datex) {
    dayx = parseInt(datex.split('/')[0]);
    monthx = parseInt(datex.split('/')[1] - 1);
    yearx = parseInt(datex.split('/')[2]);

    var d = new Date(yearx, monthx, dayx);
    calPop_obj.parent().find('strong')[0].innerHTML = dayx;
    calPop_obj.parent().find('.dayName')[0].innerHTML = "<b>" + hebMonths[monthx] + "</b>יום " + hebDays[d.getDay()];
}

function initCalendar_rus(dateID) {
    dateIDX = document.getElementById(dateID);
    var todayx = new Date;
    dayx = todayx.getDate();
    monx = todayx.getMonth() + 1;
    yearx = todayx.getFullYear();
    dateText = dayx + "." + monx + "." + yearx;
    calPop = $("#calendarPop_from" + (dateIDX.id.substr(dateIDX.id.length - 2)));
    setupCalendar_rus(calPop, dateText);

    var nextWeek = new Date(todayx.getTime() + (7 * 24 * 60 * 60 * 1000));
    dayx = nextWeek.getDate();
    monx = nextWeek.getMonth() + 1;
    yearx = nextWeek.getFullYear();
    dateText = dayx + "." + monx + "." + yearx;
    calPop = $("#calendarPop_to" + (dateIDX.id.substr(dateIDX.id.length - 2)));
    setupCalendar_rus(calPop, dateText);
}

function setupCalendar_rus(calPop_obj, datex) {
    var sep = ".";
    dayx = parseInt(datex.split(sep)[0]);
    monthx = parseInt(datex.split(sep)[1] - 1);
    yearx = parseInt(datex.split(sep)[2]);

    var d = new Date(yearx, monthx, dayx);
    calPop_obj.parent().find('strong')[0].innerHTML = dayx;
    calPop_obj.parent().find('.dayName')[0].innerHTML = "<b>" + $.datepicker.regional[''].monthNames[monthx] + "</b>" + $.datepicker.regional[''].dayNames[d.getDay()];
}

function setflightType(val) {
    if (val == "oneWay") {
        $('.dateBack').css('display', 'none');
        $('.oneWayTag').css('display', 'table-cell');
        $('#plazmaTab_formFrame_primo').removeClass('plazmaTab_formFrame_primo_diffReturn');
        $("#flightFrom").removeAttr('disabled');
        $("#flightFrom").parent().removeClass('plazmaTab_formFrame_primo_smallComboHolder_disabled');
        $('#flyTo01').attr('placeholder', 'לאן תרצו לטוס?');

        $('#flyTo01').parent().removeClass('plazmaTab_formFrame_primoDestination03');
        strongLabel = $('#flyTo01').parent().find('strong');
        strongLabel.html(strongLabel.attr('initText'));
    }
    else if (val == 'diffReturn') {
        $('.dateBack').css('display', 'table-cell');
        $('.oneWayTag').css('display', 'none');
        $('#plazmaTab_formFrame_primo').addClass('plazmaTab_formFrame_primo_diffReturn');
        $("#flightFrom").attr('disabled', 'disabled');
        $("#flightFrom").parent().addClass('plazmaTab_formFrame_primo_smallComboHolder_disabled');
        $('#flyTo01').removeAttr('placeholder');

        $('#flyTo01').parent().addClass('plazmaTab_formFrame_primoDestination03');
        strongLabel = $('#flyTo01').parent().find('strong');
        strongLabel.html(strongLabel.attr('altText'));
    }
    else {
        $('.dateBack').css('display', 'table-cell');
        $('.oneWayTag').css('display', 'none');
        $('#plazmaTab_formFrame_primo').removeClass('plazmaTab_formFrame_primo_diffReturn');
        $("#flightFrom").removeAttr('disabled');
        $("#flightFrom").parent().removeClass('plazmaTab_formFrame_primo_smallComboHolder_disabled');
        $('#flyTo01').attr('placeholder', 'לאן תרצו לטוס?');

        $('#flyTo01').parent().removeClass('plazmaTab_formFrame_primoDestination03');
        strongLabel = $('#flyTo01').parent().find('strong');
        strongLabel.html(strongLabel.attr('initText'));
    }
    setup_engineLabels();
    //	setTimeout(function(){agenciesMobileConfig();}, 50);
}

var newsTicker_timeout, newsTicker_length, newsTicker_count = 0;

function setup_newsTicker() {
    newsTicker_length = $('.newsStrip_text li').length;
    $('.newsStrip_text li').css('display', 'block');
    $('.newsStrip_text li').css('height', 'auto');
    temp = 0;
    $('.newsStrip_text li').each(function (index) { if ($(this).height() > temp) temp = $(this).height(); });
    $('.newsStrip_text li').css('height', temp + "px");
    $('.newsStrip_text li').css('display', 'none');

    $($('.newsStrip_text li')[newsTicker_count]).fadeIn(750);
    if (newsTicker_count < newsTicker_length - 1) newsTicker_count++;
    else newsTicker_count = 0;
    newsTicker_timeout = setTimeout(function () { setup_newsTicker() }, 3500);
}

function setup_engineLabels() {
    setTimeout(function () {
        labels = $('.plazmaTab_formFrame_primoDestination strong');
        labels.each(function (index) {
            labelWidth = parseInt($(labels[index]).outerWidth()) + 8;
            if ($(labels[index]).parent().find('input').exists()) {
                $(labels[index]).parent().find('input').css('text-indent', labelWidth);
            }
            else if ($(labels[index]).parent().find('.customSelectInner').exists()) {
                $(labels[index]).parent().find('.customSelectInner').css('text-indent', labelWidth);
            }
        });

        labels = $('.plazmaTab_formFrame_primo_smallComboHolder B');
        labels.each(function (index) {
            labelWidth = parseInt($(labels[index]).outerWidth()) + 15;
            if ($(labels[index]).parent().find('input').exists()) {
                $(labels[index]).parent().find('input').css('text-indent', labelWidth);
            }
            else if ($(labels[index]).parent().find('.customSelectInner').exists()) {
                $(labels[index]).parent().find('.customSelectInner').css('text-indent', labelWidth);
            }
        });
    }, 150);
}

$("body").click(function () {
    $(".calendarPop").css('display', 'none');
});

$(".calendarPop").click(function (e) { e.stopPropagation(); });
$(".dateDetails").click(function (e) { e.stopPropagation(); });
$('.crisisHolder').click(function (e) { e.stopPropagation(); });

function leftColEngine() {
    if (getViewport_width() > 670) {
        pageWidth = $('.innerContent').width();
        $('.homeMatmid_right').width(pageWidth - $('.homeMatmid_left').width() - 25);
    }
    else $('.homeMatmid_right').css('width', '');

    padLeft = parseInt($('.plazmaTab_formFrame').css('padding-left'));
    padRight = parseInt($('.plazmaTab_formFrame').css('padding-right'));
    leftCol = $('.homeMatmid_left').width();
    if (getViewport_width() > 670) $('.plazmaTab_formFrame').width(leftCol - (padLeft + padRight));
    else $('.plazmaTab_formFrame').css('width', '');

}

/* flights gallery */
function flightsGallery(galOverflow, galList, galItem, galLeft, galRight) {
    if (isIE8) {
        $(galOverflow).addClass(galOverflow.split('.')[1] + "_ie8");
        $(galList).addClass(galList.split('.')[1] + "_ie8");
    }

    this.galleryOverflow = $(galOverflow);
    this.galleryList = $(galList);
    this.galleryItem = $(galItem);
    this.galleryLeft = $(galLeft);
    this.galleryRight = $(galRight);
    this.galleryItem_length = this.galleryItem.length;
    this.flightsPlace = 0;

    this.refresh_flightsGallery = function () {
        this.galleryWidth = this.galleryOverflow.width();
        this.galleryItem.width(this.galleryWidth);
        this.galleryList.width(this.galleryItem_length * this.galleryWidth);
        this.galleryList.css('right', (-1 * (this.flightsPlace * this.galleryWidth)));
    };

    this.flightsGallery_move = function (bin) {
        if (bin) {
            if (self.flightsPlace < self.galleryItem_length - 1) {
                if (isIE8) {
                    self.flightsPlace++;
                    newRight = -1 * (self.flightsPlace * self.galleryWidth);
                    self.galleryList.animate({ right: newRight }, 500);
                }
                else {
                    self.flightsPlace++;
                    self.galleryList.css('right', (-1 * (self.flightsPlace * self.galleryWidth)));
                }
            }
        }
        else {
            if (self.flightsPlace > 0) {
                if (isIE8) {
                    self.flightsPlace--;
                    newRight = -1 * (self.flightsPlace * self.galleryWidth);
                    self.galleryList.animate({ right: newRight }, 500);
                }
                else {
                    self.flightsPlace--;
                    self.galleryList.css('right', (-1 * (self.flightsPlace * self.galleryWidth)));
                }
            }
        }

        if (self.flightsPlace == 0) {
            self.galleryRight.addClass('homeMatmid_flights_disabled');
            self.galleryLeft.removeClass('homeMatmid_flights_disabled');
        }
        else if (self.flightsPlace == self.galleryItem_length - 1) {
            self.galleryRight.removeClass('homeMatmid_flights_disabled');
            self.galleryLeft.addClass('homeMatmid_flights_disabled');
        }
        else {
            self.galleryRight.removeClass('homeMatmid_flights_disabled');
            self.galleryLeft.removeClass('homeMatmid_flights_disabled');
        }
    };

    if (isIE8) {
        var self = this;
        this.galleryLeft.on('click', function (event) { self.flightsGallery_move(true); return false; });
        this.galleryRight.on('click', function (event) { self.flightsGallery_move(false); return false; });
    }
    else {
        self = this;
        this.galleryLeft.click(function (e) { self.flightsGallery_move(true); return false; });
        this.galleryRight.click(function (e) { self.flightsGallery_move(false); return false; });
    }

    this.refresh_flightsGallery();
}

function flightsGallery_ltr(galOverflow, galList, galItem, galLeft, galRight) {
    if (isIE8) {
        $(galOverflow).addClass(galOverflow.split('.')[1] + "_ie8");
        $(galList).addClass(galList.split('.')[1] + "_ie8");
    }

    this.galleryOverflow = $(galOverflow);
    this.galleryList = $(galList);
    this.galleryItem = $(galItem);
    this.galleryLeft = $(galLeft);
    this.galleryRight = $(galRight);
    this.galleryItem_length = this.galleryItem.length;
    this.flightsPlace = 0;

    this.refresh_flightsGallery = function () {
        this.galleryWidth = this.galleryOverflow.width();
        this.galleryItem.width(this.galleryWidth);
        this.galleryList.width(this.galleryItem_length * this.galleryWidth);
        this.galleryList.css('left', (-1 * (this.flightsPlace * this.galleryWidth)));
    };

    this.flightsGallery_move = function (bin) {
        if (bin) {
            if (self.flightsPlace < self.galleryItem_length - 1) {
                if (isIE8) {
                    self.flightsPlace++;
                    newLeft = -1 * (self.flightsPlace * self.galleryWidth);
                    self.galleryList.animate({ left: newLeft }, 500);
                }
                else {
                    self.flightsPlace++;
                    self.galleryList.css('left', (-1 * (self.flightsPlace * self.galleryWidth)));
                }
            }
        }
        else {
            if (self.flightsPlace > 0) {
                if (isIE8) {
                    self.flightsPlace--;
                    newLeft = -1 * (self.flightsPlace * self.galleryWidth);
                    self.galleryList.animate({ left: newLeft }, 500);
                }
                else {
                    self.flightsPlace--;
                    self.galleryList.css('left', (-1 * (self.flightsPlace * self.galleryWidth)));
                }
            }
        }

        if (self.flightsPlace == 0) {
            self.galleryLeft.addClass('homeMatmid_flights_disabled');
            self.galleryRight.removeClass('homeMatmid_flights_disabled');
        }
        else if (self.flightsPlace == self.galleryItem_length - 1) {
            self.galleryLeft.removeClass('homeMatmid_flights_disabled');
            self.galleryRight.addClass('homeMatmid_flights_disabled');
        }
        else {
            self.galleryLeft.removeClass('homeMatmid_flights_disabled');
            self.galleryRight.removeClass('homeMatmid_flights_disabled');
        }
    };

    if (isIE8) {
        var self = this;
        this.galleryLeft.on('click', function (event) { self.flightsGallery_move(false); return false; });
        this.galleryRight.on('click', function (event) { self.flightsGallery_move(true); return false; });
    }
    else {
        self = this;
        this.galleryLeft.click(function (e) { self.flightsGallery_move(false); return false; });
        this.galleryRight.click(function (e) { self.flightsGallery_move(true); return false; });
    }

    this.refresh_flightsGallery();
}



var newsTicker_timeout, newsTicker_length, newsTicker_count = 0;
function setup_newsTicker() {
    newsTicker_length = $('.newsStrip_text li').length;
    $('.newsStrip_text li').css('display', 'block');
    $('.newsStrip_text li').css('height', 'auto');
    temp = 0;
    $('.newsStrip_text li').each(function (index) { if ($(this).height() > temp) temp = $(this).height(); });
    $('.newsStrip_text li').css('height', temp + "px");
    $('.newsStrip_text li').css('display', 'none');

    $($('.newsStrip_text li')[newsTicker_count]).fadeIn(750);
    if (newsTicker_count < newsTicker_length - 1) newsTicker_count++;
    else newsTicker_count = 0;
    newsTicker_timeout = setTimeout(function () { setup_newsTicker() }, 3500);
}

function pointsAmount_greyFrame() {
    if (getViewport_width() > 670) {
        equalHeights($('.pointsAmount_title'));
        equalHeights($('.pointsAmount_body'));
    }
    else {
        $('.pointsAmount_title').css('height', '');
        $('.pointsAmount_body').css('height', '');
    }
}

function toggle_pointGraph(objLink, objBin) {
    $(objLink).parent().find('A').removeClass('pointsAmount_graphTabs_active');
    $(objLink).addClass('pointsAmount_graphTabs_active');

    $('.pointsAmount_graphHolder').hide();
    $($('.pointsAmount_graphHolder')[objBin]).show();
}

function toggle_activityTable_form(val) {
    if (val == 'choose') {
        $('.activityTable_formDates').show();
        $('.activityTable_formDates_notes').show();
    }
    else {
        $('.activityTable_formDates').hide();
        $('.activityTable_formDates_notes').hide();
    }
}

function activityList() {
    $('.activityTable_line').each(function (index) {
        $(this).on("click", function (e) {

            if (!($(this).hasClass('activityTable_line_opened'))) {
                $(this).addClass('activityTable_line_opened');
                if (($(this).next().hasClass('DinamicOpenTableMyflights'))) {
                    $(this).next().css('width', $(this).width() + 'px');
                    $(this).next().css('display', 'block');
                }
                else {
                    if (getViewport_width() > 800) {
                        $(this).next().css('display', 'table-row');

                    }
                    else

                        $(this).next().css('display', 'block');
                }
                $(this).find('.activityTable_plus').removeClass('activityTable_plus').addClass('activityTable_minus');
            }
            else {



                $(this).removeClass('activityTable_line_opened');
                $(this).next().css('display', '');
                $(this).find('.activityTable_minus').removeClass('activityTable_minus').addClass('activityTable_plus');
            }

            return false;
        });
    });
}

function refresh_activityList() {
    $('.activityTable_opened').each(function (index) {
        if ($(this).css('display') != "none") {
            if (getViewport_width() > 800) {
                $(this).css('display', 'table-row');

            }
            else
                $(this).css('display', 'block');
        }
    });
}

function equalize_actionPanel() {
    if (getViewport_width() > 670) {

        rightHeight = $('.homeMatmid_benefits_list').height();
        leftLinkHeight = $('.homeMatmid_services_flightUpdate').outerHeight(true) + 2;
        $('.homeMatmid_services_list').height(rightHeight - leftLinkHeight);
    }
    else $('.homeMatmid_services_list').css('height', '');
}

function additionalProducts_heights() {
    if (getViewport_width() > 568) {
        $('.additionalProducts_list .hotDeals_itemText').css("min-height", "205px");
        var elementHeights = $('.additionalProducts_list .hotDeals_itemText').map(function () {
            return $(this).height();
        }).get();

        var maxHeight = Math.max.apply(null, elementHeights);
        if (maxHeight > 205)
            $('.additionalProducts_list .hotDeals_itemText').css("min-height", maxHeight + "px");
    }
    else {
        $('.additionalProducts_list .hotDeals_itemText').css("min-height", "0px");
    }
}

function checkTitleStrip() {
    $('.hotDeals_title').each(function (index) {
        $(this).parent().css('height', $(this).outerHeight());

    });
}

function homeBanner_height() {
    if (getViewport_width() > 670) {
        $('.homeMatmid_bannerBG').height($('.plazmaTab_formFrame').outerHeight());
        $('.homeMatmid_bannerContent').height($('.homeMatmid_bannerBG').height());
        $('.homeMatmid_bannerBG img').height($('.homeMatmid_bannerBG').height());
    }
    else {
        $('.homeMatmid_bannerBG').css('height', '');
        $('.homeMatmid_bannerContent').css('height', '');
        $('.homeMatmid_bannerBG img').css('height', '');

        $('.homeMatmid_bannerBG img').height($('.homeMatmid_bannerContent').height() + 20);
    }
}

var isCrisisNeeded = true;
function toggleCrisis() {
    if ($(".crisisHolder").exists()) {
        if (!($('.crisisHolder').hasClass('crisisHolder_small'))) {
            $('.crisisHolder').addClass('crisisHolder_small');
            setTimeout(function () { crisisBanner(); }, 50);
        }
    }
    isCrisisNeeded = false;
}

var isCrisis2Active = true;
function toggleCrisis_info() {
    if ($(".crisisHolder_small_info").exists()) {
        if (!($('.crisisHolder').hasClass('crisisHolder_small_info_minimized'))) {
            $('.crisisHolder').addClass('crisisHolder_small_info_minimized');
            setTimeout(function () { crisisBanner(); }, 50);
            isCrisis2Active = false;
        }
        else {
            $('.crisisHolder').removeClass('crisisHolder_small_info_minimized');
            setTimeout(function () { crisisBanner(); }, 50);
            isCrisis2Active = true;
        }
    }
}

function crisisBanner() {
    temp = $('.crisisCentered').outerHeight();
    $('.crisisHolder').css('height', temp + "px");
    if ($(".plazmaHolder").exists()) setPlazmaTop();
}

$("body").click(function () {
    if (isCrisisNeeded) toggleCrisis();
    if (isCrisis2Active) toggleCrisis_info();
});

$(window).scroll(function () {
    if (isCrisisNeeded) toggleCrisis();
    if (isCrisis2Active) toggleCrisis_info();
});

function getScrollTop() {
    var ScrollTop = document.body.scrollTop;
    if (ScrollTop == 0) {
        if (window.pageYOffset) ScrollTop = window.pageYOffset;
        else ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
    }
    return ScrollTop;
}

function popupToggle(objBin, objPop) {
    if (objBin) {
        $('.liteBox').show();
        $('.' + objPop).css('top', parseInt($('.' + objPop).css('top')) + getScrollTop());
        $('.' + objPop).show();
    }
    else {
        $('.liteBox').hide();
        $('.' + objPop).hide();
        $('.' + objPop).css('top', '');
    }
}

function dropDown_value(objLink, objText) {
    $(objLink).closest('.greyFrame_formQuestion').find('a.greyFrame_dropDown_toggle span span').html(objText);
    $('.greyFrame_dropDown').removeClass('greyFrame_dropDown_active');
}

function toggleDropDown(objLink) {
    if ($(objLink).parent().hasClass('greyFrame_dropDown_active')) {
        $('.greyFrame_dropDown').removeClass('greyFrame_dropDown_active');
    }
    else {
        $('.greyFrame_dropDown').removeClass('greyFrame_dropDown_active');
        $(objLink).parent().addClass('greyFrame_dropDown_active');
        $(objLink).parent().find('.greyFrame_dropDown_drawer').css('top', $('.greyFrame_dropDown').height());
    }
}

function editMail() {
    $(".divurPop_form").addClass("divurPop_form_edit");
}

$("body").click(function () {
    $('.greyFrame_dropDown').removeClass('greyFrame_dropDown_active');
});

$(".greyFrame_dropDown_toggle").click(function (e) { e.stopPropagation(); });


var sendMail = false;
function myAccount_sendMail(objLink) {
    tempText = $(objLink).attr('altText');
    $(objLink).attr('altText', $(objLink).html());
    $(objLink).html(tempText);

    if (($('.sendByMail').hasClass('sendByMail_active')) || ($('.sendByMail').hasClass('sendByMail_after'))) {
        $('.sendByMail').removeClass('sendByMail_active');
        $('.sendByMail').removeClass('sendByMail_after');
    }
    else { $('.sendByMail').addClass('sendByMail_active'); }
}

function myAccount_sendMail_success() {
    $('.sendByMail').removeClass('sendByMail_active');
    $('.sendByMail').addClass('sendByMail_after');
}


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



////////////////////////////////////elal interactive page ////////////////////////////////////////////




window.onload = function () {


    /////////////////make the hotDeals be in the middle at startup////////////////////////////////


    //////var qwe = '-' + (((($('.hotDeals_double').width() * 5) - window.innerWidth) / 2) - 40 + 'px');
    //////$('.homeMatmid_hotDeals_list').css('transform', 'translate(' + qwe + ', 0px) translateZ(0px)');


    //////if (innerWidth < 480) {
    //////    var qwe = '-' + (((($('.hotDeals_double').width() * 5) - window.innerWidth) / 2) + 30 + 'px');
    //////    $('.homeMatmid_hotDeals_list').css('transform', 'translate(' + qwe + ', 0px) translateZ(0px)');
    //////}

    //////if (innerWidth < 360) {
    //////    var qwe = '-' + (((($('.hotDeals_double').width() * 5) - window.innerWidth) / 2) + 40 + 'px');
    //////    $('.homeMatmid_hotDeals_list').css('transform', 'translate(' + qwe + ', 0px) translateZ(0px)');
    //////}












    //$(document).on('click', '#page5 .ScoreBoard.TopBarLinksPage5', function (event) {
    //     $("#page5 .TopPointsPage5_innerlinks").animate({
    //        height: 'toggle',

    //    });
    //    $('.DestinationsWrapper.gradient').toggleClass('LowOpacity');
    //});


    //////////////var handleClick = 'ontouchstart' in document.documentElement ? 'touchstart' : 'click';
    //////////////$(document).on(handleClick, '#page5 .ScoreBoard.TopBarLinksPage5', function () {
    //////////////     $("#page5 .TopPointsPage5_innerlinks").animate({
    //////////////        height: 'toggle',

    //////////////    });
    //////////////    $('.DestinationsWrapper.gradient').toggleClass('LowOpacity');
    //////////////});








    //////////////////$("#page5 .ScoreBoard.TopBarLinksPage5").click(function () {

    //////////////////    $('#page5 .TopPointsPage5_innerlinks').toggleClass('GoUp');
    //////////////////    $('.DestinationsWrapper.gradient').toggleClass('LowOpacity');

    //////////////////});








    //$("#page5 .ScoreBoard.TopBarLinksPage5").show();
    //$("#page5 .TopPointsPage5_innerlinks").hide();

    //$('#page5 .ScoreBoard.TopBarLinksPage5').toggle(

    //function () {
    //    alert('sd');

    //    $('#page5 .TopPointsPage5_innerlinks').animate({
    //        height: "150",
    //        padding: "20px 0",
    //        top: "0",
    //        backgroundColor: '#000000',
    //        opacity: .8
    //    }, 500);
    //},
    //function () {
    //    $('#panel').animate({
    //        height: "0",
    //        padding: "0px 0",
    //        opacity: .2
    //    }, 500);
    //});




    //calling the function with spacific cloud 
    function InteractiveCloud(cloud) {
        //var cloud = ".bg";
        $(cloud).interactive_bg();
    }

    //call for action - clouds   
    //$(".bg").interactive_bg();
    InteractiveCloud(".bg");
    InteractiveCloud(".bg_WhereTo");
    InteractiveCloud(".bg_theSun");


    $('#up-arrow').click(function () {
        var index = $("#myScrollspy ul li").index($("#myScrollspy ul li.active"));
        if (index > 1) {
            $("#myScrollspy ul li").eq(index - 1).find('a').click();
        }

    });


    //$(window).scroll(function () {
    //    //alert("Hello");
    //    var iCurScrollPos = $(this).scrollTop();
    //    if (iCurScrollPos > iScrollPos) {
    //        //Scrolling Down
    //        alert("Scrolling Down");
    //    } else {
    //        //Scrolling Up
    //        alert("Scrolling Up");
    //    }
    //    iScrollPos = iCurScrollPos;
    //});


    //////////page2/////////////
    $("a.page2,a.page2.page2Link.ui-link, a.innerLinkResponsive.page2.ui-link").click(function () {
        $('html, body').animate({ scrollTop: ($('#page2').offset().top) }, 1500);
        makeBottomPointsToTop();
    });


    //if (window.innerWidth < 480) {
    //    $(".elalInteractivePageBody input.ui-slider-input,input#slider-2").focus(function () {
    //        // $(this).focus();
    //        $(".elalInteractivePageBody input.ui-slider-input").select();
    //        this.select();
    //    });
    //}


    $(function () {
        var focusedElement;
        $(document).on('focus', 'input', function () {
            if (focusedElement == this) return; //already focused, return so user can now place cursor at specific point in input.
            focusedElement = this;
            setTimeout(function () { focusedElement.select(); }, 50); //select all text in any field on focus for easy re-entry. Delay sightly to allow focus to "stick" before selecting.
        });
    });


    //////////page3/////////////
    if (window.innerWidth < 868) {
        $("a.page3,a.page3.page3Link.ui-link,a.innerLinkResponsive.page3.ui-link").click(function () {
            $('html, body').animate({ scrollTop: ($('.MainHeaderPage1.page3').offset().top - 20) }, 1500);
        });
    }
    else {
        $("a.page3,a.page3.page3Link.ui-link,a.innerLinkResponsive.page2.ui-link").click(function () {

            $('html, body').animate({ scrollTop: ($('#page3').offset().top - 20) }, 1500);
        });
    }


    //////////page4/////////////
    if (window.innerWidth < 868) {

        $("#page4 .MainButtonPage1,a.page4.page4Link.ui-link,a.innerLinkResponsive.page4.ui-link").click(function () {

            $('html, body').animate({ scrollTop: ($('#page4').offset().top - 30) }, 1500);
        });
    }

    else {
        $("#page4 .MainButtonPage1,a.page4.page4Link.ui-link,a.innerLinkResponsive.page4.ui-link").click(function () {

            $('html, body').animate({ scrollTop: ($('#page4').offset().top - 80) }, 1500);
        });
    }

    //////////page5/////////////
    $("a.page5 ,a.page5.page5Link.ui-link,a.innerLinkResponsive.page5.ui-link").click(function () {
        $('html, body').animate({ scrollTop: ($('#page5').offset().top - 40) }, 1500);
    });





    //InteractiveCloud(".cloud1");

    init();
}



/////////// calling top chart buttons  ////////////////////////


function makeBottomPointsToTop() {
    if (window.innerWidth > 768) {
        $(".ButtomPointsPage1").animate({
            top: 103,
        }, {
            duration: 1500,
            specialEasing: {
                width: "linear",
                height: "ease"
            }
        });

        //setTimeout(function () {
        //    $('.HalfYearEnding').fadeIn();
        //}, 3500);


        $(".HalfYearEnding").animate({
            top: 194,
            opacity: 1,
        }, {
            duration: 1500,
            specialEasing: {
                width: "linear",
                height: "ease"
            }
        });


        $('.ButtomPointsPage1_wrapper .HalfYearEnding span').css('display', 'block').css('opacity', '1').fadeIn(1500);
    }
    else {
        $('.ButtomPointsPage1').css('top', '0px').css('border-bottom', 'solid 2px #00003C').fadeIn(1500);
        $('.HalfYearEnding').css('top', '-15px').css('display', 'block').css('position', 'relative').css('opacity', '1').fadeIn(1500);
        $('.ButtomPointsPage1_wrapper .HalfYearEnding span').css('display', 'block').css('opacity', '1').fadeIn(1500);
    }
}


function ScrollToTop() {
    setTimeout(function () {
        $('#page2,#page3,#page4,#page1').css('display', 'block');
        $('html, body').animate({ scrollTop: 0 });

    },
    150);
    //location.reload();
    $('.ButtomPointsPage1_wrapper').css('position', 'relative');
}



function makeBottomPointsToBottom() {
    $('.ButtomPointsPage1').fadeOut("slow");
    $('.ButtomPointsPage1').css('top', 'inherit').css('bottom', '0px').css('border-bottom', 'solid 0px #00003C').fadeIn("slow");

    //$('.HalfYearEnding').css('top', 'inherit').fadeIn("slow");
    $('.ButtomPointsPage1_wrapper').css('position', 'relative');
}

function makeBottomPointsTofadeOut() {
    //$('.ButtomPointsPage1').fadeOut("slow");
    //$('.HalfYearEnding').fadeOut("slow");
    //$('.ButtomPointsPage1_wrapper').css('position', 'relative');
    ////$('.ButtomPointsPage1').css('bottom', '0px').css('border-bottom', 'solid 0px #00003C').fadeIn("slow");
}

function makeBottomPointsHide() {
    //alert();
    // $('.ButtomPointsPage1_wrapper').css('position', 'absolute').fadeOut("slow");
}


$(document).load(function () {

    ////if (($(window).scrollTop() <= $('#page2').offset().top)) {
    ////    // alert('window.innerHeight #page2  :' + window.innerHeight);
    ////    alert('123');
    ////    //  $('html, body').animate({ scrollTop: ($('#page2').offset().top) }, 1500);
    ////    //  makeBottomPointsToTop();
    ////}

    ////$('html, body.elalInteractivePageBody.ui-mobile-viewport.ui-overlay-a').css({
    ////    'overflow': 'hidden !important'
    ////});

});


$(document).ready(function () {









    // $('.elalInteractivePageBody').css('overflow', 'hidden');
    //show/hide scoreBoard and elements that reach a certin point in the page
    function scroll_style() {
        ////////if (($(window).scrollTop() <= $('#page2').offset().top)) {
        ////////    // alert('window.innerHeight #page2  :' + window.innerHeight);
        ////////    ////////alert('123');
        ////////    //$('html, body').animate({ scrollTop: ($('#page2').offset().top) }, 1500);
        ////////    //makeBottomPointsToTop();
        ////////}
        //////if (($(window).scrollTop() < $('#page2').offset().top)) {
        //////    // alert('window.innerHeight #page3  :' + window.innerHeight);

        //////}
        //////if ($(window).scrollTop() > ($('#page4').offset().top)) {
        //////    // alert('window.innerHeight #page4  :' + window.innerHeight);
        //////}
        ////if (window.innerWidth < 520) {
        ////    var innerHeight = (window.innerHeight - 100);

        if (innerHeight > 500 && innerWidth < 800) {
            //&& innerWidth > 900
            $('#page2').css('height', innerHeight - $('.centerheader').height());
            $('#page3').css('height', innerHeight - $('.centerheader').height());
            $('#page4').css('height', innerHeight - $('.centerheader').height());

            $('.bg').css('height', innerHeight - $('.centerheader').height());
            $('#bg_Snow').css('height', innerHeight - $('.centerheader').height() + 150);
            $('.bg_WhereTo').css('height', innerHeight - $('.centerheader').height());
        }
        else {
            $('#page2').css('height', '800px');
            $('#page3').css('height', '800px');
            $('#page4').css('height', '800px');

            $('.bg').css('height', '800px');
            $('#bg_Snow').css('height', '950px');
            $('.bg_WhereTo').css('height', '800px');
        }

        //$('#page2').css('height', innerHeight);
        //$('#page3').css('height', innerHeight);
        //$('#page4').css('height', innerHeight); 



        //$('#page4 .bg').css('height', 'auto !important');
        //$('#page5 .bg').css('height', 'auto !important');

        ////}

        if ($(window).scrollTop() > ($('#page5').offset().top) - 120) {
            //alert('window.innerHeight' + window.innerHeight);
            $('.TopPointsPage5_wrapper').fadeIn(800).css('display', 'block');
            $('.ButtonBlockToTop').fadeIn(800).css('display', 'block');
            $('.ButtomPointsPage1_wrapper').fadeIn(800).css('display', 'none');
            $('.elalInteractivePageBody').css('overflow', 'auto !important');
            //$('#page2,#page3,#page4,#page1').css('display', 'none');
            // $('html, body').animate({ scrollTop: 0 });
        }
        else {
            $('.TopPointsPage5_wrapper').fadeOut(50).css('display', 'none');
            $('.ButtonBlockToTop').fadeOut(1500).css('display', 'none');
            // $('.elalInteractivePageBody').css('overflow', 'hidden');
            // scrolledPast = true;
            //var slide1 = ($('#page5').offset().top) - 30;
            //$("html, body").animate({ scrollTop: slide1 }, "slow");
        }

        if ($(window).scrollTop() > ($('#page3').offset().top) - 200) {
            $('.HalfYearEnding').fadeOut("slow");
            $('.ButtomPointsPage1').fadeOut("slow");
        }
        else {
            $('.ButtomPointsPage1').fadeIn("slow").css('display', 'block');
            //$('.ButtomPointsPage1').fadeIn("slow").css('position', 'absolute');
        }

        if ($(window).scrollTop() > ($('#page4').offset().top) - 200) {
            $('.ButtomPointsPage1_wrapper').css('position', 'absolute');
        }
        else {
            $('.ButtomPointsPage1_wrapper').css('position', 'relative');
        }



    }

    $(function () {
        $(window).scroll(scroll_style);
        scroll_style();
    });



    if (window.innerWidth < 768) {
        $(".TopPointsPage5_wrapper").hover(
               function () {
                   $('a.innerLinkResponsive.OpenClose.ui-link img').css("content", "url('imgs/page5_close.png')");
                   $('#page5 .TopPointsPage5_wrapper .ScoreBoard:first-child').toggleClass('open')
                   $('#page5 .TopPointsPage5_innerlinks').css("display", "block");

                   $('html, body').css({
                       'overflow': 'hidden',
                       'height': '100%'
                   });

               }, function () {
                   $('a.innerLinkResponsive.OpenClose.ui-link img').css("content", "url('imgs/page5_open.png')");
                   $('#page5 .TopPointsPage5_wrapper .ScoreBoard:first-child').toggleClass('open');
                   $('html, body').css({
                       'overflow': 'auto',
                       'height': 'auto'
                   });
               }
            );
    }


    $("#page5 .TopPointsPage5_wrapper .ScoreBoard:first-child a").click(function () {
        // $('#page5 .TopPointsPage5_innerlinks').css("display", "none");


        if ($("#page5 .TopPointsPage5_innerlinks").hasClass("active")) {
            $("#page5 .TopPointsPage5_innerlinks").removeClass("active");
            $('#page5 .TopPointsPage5_innerlinks').css("display", "block");
            $('a.innerLinkResponsive.OpenClose.ui-link img').css("content", "url('imgs/page5_close.png')");

            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
            $(document).bind('touchmove', false); $("donotscrollme").on("touchmove", false);

        }
        else {
            $("#page5 .TopPointsPage5_innerlinks").addClass("active");
            $('#page5 .TopPointsPage5_innerlinks').css("display", "none");
            $('a.innerLinkResponsive.OpenClose.ui-link img').css("content", "url('imgs/page5_open.png')");
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
            $(document).bind('touchmove', true); $("donotscrollme").on("touchmove", true);
        }
    });




    //var init = function () {
    //    var card = $('.DestinationsBox')  //document.getElementById('card');

    //    document.getElementById('flip').addEventListener('click', function () {
    //        card.toggleClassName('flipped');
    //    }, false);
    //};

    window.addEventListener('DOMContentLoaded', init, false);



    //$(window).scroll(function () {
    //    if ($(window).scrollTop() > 3300) {
    //        // alert($(window).scrollTop());


    //        $('.TopPointsPage5_wrapper').fadeIn(800).css('display', 'block');
    //        $('.ButtonBlockToTop').fadeIn(800).css('display', 'block');

    //        $('.ButtomPointsPage1_wrapper').fadeIn(800).css('display', 'none');


    //    }
    //    else {
    //        $('.TopPointsPage5_wrapper').fadeOut(50).css('display', 'none');
    //        $('.ButtonBlockToTop').fadeOut(1500).css('display', 'none');


    //    }
    //});



    /// calling the border limit div of the choose points page 2 
    $("#thePoints .ui-slider-bg.ui-btn-active").append("<div class='overUiSlider'><div class='popUpText'><span>הנקודות שברשותי</span></div></div>");




    /// calling the background for the border limit div of the choose points page 2 
    $("#thePoints .ui-slider-bg.ui-btn-active").append("<div class='overUiSliderBackground'><div class='popUpTextBackground'> </div></div>");



    ////////////en//////////////

    $("#thePointsEn  .ui-slider-bg.ui-btn-active ").append("<div class='overUiSlider'><div class='popUpText'><span>   Points I Earned</span></div></div>");
    $("#thePointsEn .ui-slider-bg.ui-btn-active").append("<div class='overUiSliderBackground'><div class='popUpTextBackground'> </div></div>");



    //$("#thePoints ").append("<div class='backOverground'> </div>");

    /// setting points that i oun to stick to points handle ///

    ////////$('a.ui-slider-handle.ui-btn.ui-shadow').mousemove(function (event) {
    ////////    //alert('alert');
    ////////    // var handleWidth = $('a.ui-slider-handle.ui-btn.ui-shadow').css('left');

    ////////    var PointsWidth = $('#page2 .overUiSliderBackground').css('width');
    ////////    var PointsWidthLimit = $('#page2 .overUiSlider').css('width');
    ////////    var handle = $('a.ui-slider-handle.ui-btn.ui-shadow').css('left');

    ////////    if (handle < PointsWidthLimit ) {
    ////////        //var handle = $('a.ui-slider-handle.ui-btn.ui-shadow').css('left');
    ////////        $('#page2 .overUiSliderBackground').css('width', handle);
    ////////    }
    ////////    else  {
    ////////        $('#page2 .overUiSliderBackground').css('width', PointsWidthLimit);
    ////////    }
    ////////    //alert($('a.ui-slider-handle.ui-btn.ui-shadow').css('left'));
    ////////});



    $('.elalInteractivePageBody .ui-slider-track .ui-btn.ui-slider-handle').mousemove(function (event) {
        //alert('alert');
        // var handleWidth = $('a.ui-slider-handle.ui-btn.ui-shadow').css('left');

        var PointsWidth = $('#page2 .overUiSliderBackground').css('width'); //light blue background
        var PointsWidthLimit = $('#page2 .overUiSlider').css('width'); //// static indicator for points we have
        var handle = $('.ui-page-theme-a .ui-slider-track .ui-btn-active').css('width'); // dark blue background



        //PointsWidth = PointsWidthLimit;

        if (parseInt(handle) <= parseInt(PointsWidthLimit)) {
            //var handle = $('a.ui-slider-handle.ui-btn.ui-shadow').css('left');
            $('#page2 .overUiSliderBackground').css('width', handle);
        }
        else if (parseInt(handle) > parseInt(PointsWidthLimit)) {
            $('#page2 .overUiSliderBackground').css('width', PointsWidthLimit);
        }
        //alert($('a.ui-slider-handle.ui-btn.ui-shadow').css('left'));
    });







    ///////////page steps


    //$('#page2').css('visibility', 'hidden');
    //$('#page3').css('visibility', 'hidden');
    //$('#page4').css('visibility', 'hidden');
    //$('#page5').css('visibility', 'hidden');



    //$('#page1 .MainButtonPage1').click(function (e) {
    //    e.preventDefault();
    //    $('#page2').css('visibility', 'visible');
    //    $('#page3').css('visibility', 'visible');
    //    $('#page4').css('visibility', 'visible');
    //    $('#page5').css('visibility', 'visible');

    //    $('.page2Ancor').attr('href','#page2').click();
    //});




    //$('#page2 .MainButtonPage1').click(function (e) {
    //    e.preventDefault();
    //    $('#page3').css('display','block');
    //});


    //$('#page3 .MainButtonPage1').click(function (e) {
    //    e.preventDefault();
    //    $('#page4').css('display', 'block');
    //});


    //$('#page4 .MainButtonPage1').click(function (e) {
    //    e.preventDefault();
    //    $('#page5').css('display', 'block');
    //});


    //$('#page5 .MainButtonPage1').click(function (e) {
    //    e.preventDefault();
    //    setTimeout(
    //        " $('#page2').$('#page3').$('#page4').$('#page5').css('display', 'none');"
    //        , 3000);

    //});









    // error and background change to the input for choosing the points page2
    $('.elalInteractivePageBody input.ui-slider-input').change(function () {
        // alert();
        if ($('input#slider-2').val() >= 7499) {
            $('input#slider-2').addClass('active');
            this.select();
        }
    });


    //$(".ErrorPoints").css('display', 'none');


    $.fn.digits = function () {
        return this.each(function () {
            $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        })
    }


    // error and background change to the input for choosing the points page2
    $('input#slider-2').change(function () {
        //alert();
        ///////////////////////// $("#inner-editor").digits();
        if ($('input#slider-2').val() >= 7500) {
            //input#slider-2::selection {
            //    background: red;
            //}
            $(".ErrorPoints").css('display', 'block !important');

            $('input#slider-2').addClass('active');
            this.select();

        }
        else {
            //
            $('input#slider-2').removeClass('active'); $(".ErrorPoints").css('display', 'block !important');
        }
    });





    //$("input#slider-2").focus(function () {
    //    if ($('input#slider-2').val() >= 7500) {
    //        this.select().css('','');
    //    }
    //});




    ////////////adding comma to the points slider ///////////


    function addComma() {
        tempText = $('#Slider1 .tooltip-inner').text();
        NewtempText = (tempText).replace(/\d(?=(?:\d{3})+$)/g, '$&,');
        $('#Slider1 .tooltip-inner').text(" ");
        $('#Slider1 .tooltip-inner').text(NewtempText);
        tempText = " ";
        $('#Slider1 .tooltip-inner').text(tempText);
        $('#Slider1 .tooltip-inner').text(NewtempText);
        NewtempText = " ";
        // $('#txt').text() = txt.value.replace(",", "").replace(/(\d+)(\d{3})/, "$1,$2");
    }
    function ClearComma() {
    }





    ///////////  moving vertically the pages with table view ease animation   ////////////////////////

    ////////////function verticallyPages() {

    ////////////    function filterPath(string) {
    ////////////        return string
    ////////////          .replace(/^\//, '')
    ////////////          .replace(/(index|default).[a-zA-Z]{3,4}$/, '')
    ////////////          .replace(/\/$/, '');
    ////////////    }

    ////////////    $('a[href*=#]').each(function () {
    ////////////        if (filterPath(location.pathname) == filterPath(this.pathname)
    ////////////        && location.hostname == this.hostname
    ////////////        && this.hash.replace(/#/, '')) {
    ////////////            var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) + ']');
    ////////////            var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
    ////////////            if ($target) {
    ////////////                var targetOffset = $target.offset().top;
    ////////////                $(this).click(function () {
    ////////////                    $('html, body').animate({ scrollTop: targetOffset }, 1500);
    ////////////                    return false;
    ////////////                });
    ////////////            }
    ////////////        }
    ////////////    });
    ////////////}

    ////////////verticallyPages();








    ////ticks in the slider picture page 2 interactive cloud////////////  ////ticks for the range points meter :

    function ticks() {
        var ticks = '<div class="sliderTickmarks "><span>0</span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span>3,750</span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span> </span></div>';
        ticks += '<div class="sliderTickmarks "><span>7,500</span></div>';
        $(".ui-slider .ui-slider-track").prepend(ticks);
    }

    ticks();



    // Function that making the buttom boxes stick to their upper boxes 

    // In #page5 interactive page - we need to make the boxes get close to each other from buttom to top 

    //if (getViewport_width() > 800) {

    $(".DestinationsBox").each(function () {

        var FirstLoopBox = $(this);

        //console.log(ccc.position()); 
        var DesiredTopPosition;
        var NewTopPosition;
        $(".DestinationsBox").each(function () {
            if (FirstLoopBox.attr('id') != $(this).attr('id')) {
                var SecondLoopBox = $(this);
                if (SecondLoopBox.position().left == FirstLoopBox.position().left && FirstLoopBox.position().top > SecondLoopBox.position().top) {


                    DesiredTopPosition = SecondLoopBox.position().top + SecondLoopBox.height() + 20;
                }
            }
        });
        //$(this).css('top', NewTopPosition);
        NewTopPosition = DesiredTopPosition - $(this).position().top
        $(this).css({ top: NewTopPosition });
    });
    // after the function compleets looping trough all the boxes we need to excecute the Flip animation on the boxes adding the Flip class
    $('.DestinationsBox.Flipable').addClass('Flip');

    //}



 


    // End Function 


    $('.homeMatmid_hotDeals_Continew_button').addClass('inactive');

    $('.hotDeals_item').click(function (e, f) {
        e.preventDefault();

        $(this).toggleClass('active');
        $(this).toggleClass('stay-up');

        if ($('.hotDeals_item').hasClass('active')) {
            $('.homeMatmid_hotDeals_Continew_button').addClass('active');
            $('.homeMatmid_hotDeals_Continew_button').removeClass('inactive');

            $(".elalInteractivePageBody .homeMatmid_hotDeals_Continew_button").attr("href", "#page5");

            // $(this).attr('href') = 'page5';
        }
        else {
            $('.homeMatmid_hotDeals_Continew_button').removeClass('active');
            $('.homeMatmid_hotDeals_Continew_button').addClass('inactive');
            $("a.FindDestinations").removeAttr('href');
        }
    });



    jQuery('#page5 .DestinationsWrapper .DestinationsBox.Flip').click(function () {
        jQuery(this).toggleClass('active');
    });



    //elalinteractive calling all destinations to the search options

    //jQuery('.elalInteractivePageBody .CallingAllDestinations').click(function (e, f) {
   
    //});


    $(".CallingAllDestinations").click(function (e, f) {
        // alert(this);
        //$('.hotDeals_item').addClass('active').addClass( 'stay-up');
         
        e.preventDefault();
       
        
        if ($(".CallingAllDestinations").hasClass('active')) {
           
        }
        else {
           
        }



        //if ($('.vi-sign_small').css('display') == 'none') {
           
        //} else {
           
        //}

        
        if (($('.hotDeals_item.enabled').hasClass('active')) && ($(".CallingAllDestinations").hasClass('active'))) {
            $(this).removeClass('active');
            $('.vi-sign_small').hide();


            $('.hotDeals_item.enabled').removeClass('active').removeClass('stay-up');
            $('.homeMatmid_hotDeals_Continew_button').addClass('inactive');
            $('.homeMatmid_hotDeals_Continew_button').removeClass('active');
            $(".elalInteractivePageBody .homeMatmid_hotDeals_Continew_button").attr("href", "#page5");
        }
        else {
            $(this).addClass('active');
            $('.vi-sign_small').show();


            $('.hotDeals_item.enabled').addClass('active').addClass('stay-up');
            $('.homeMatmid_hotDeals_Continew_button').addClass('active');
            $('.homeMatmid_hotDeals_Continew_button').removeClass('inactive');
            $("a.FindDestinations").removeAttr('href');
        }
    });


    $(".myFlights_topTabs ul li a").click(function () {
        $('.myFlights_topTabs ul li a').removeClass("myFlights_topTabs_active");
        $(this).toggleClass("myFlights_topTabs_active");
    });

    if (getViewport_width() > 800) {
        $('.myflights .activityTable_list li.activityTable_opened span.activityTable_openedDetails').removeClass('myflights_activityTable_list_mobile_span');
        $('.activityTable_list li.activityTable_opened span.activityTable_openedDetails P').removeClass('activityTable_list_P_mobile_openedDetails_P');
    }
    else {
        $('.myflights .activityTable_list li.activityTable_opened span.activityTable_openedDetails').addClass('myflights_activityTable_list_mobile_span');
        $('.activityTable_list li.activityTable_opened span.activityTable_openedDetails P').addClass('activityTable_list_P_mobile_openedDetails_P');
    }

    //////////////  click Calculate movePoints ////////////////////////

    function HowMuchForYourPoints(num) {
        var ToPAy = 0;
        ToPAy = (num * 0.2) + 15;
        return ToPAy;
    }

    //$(".clickToCalculate").unbind("click");
    $('span.CostOfTransferMoney').text('0');
    $('span.MoneyToPayFinal').text('0');
    $('.PointsToDeclareFinal').text('0');

    $(".clickToCalculate").click(function () {

        if (($('input.PointsInput').val()) != '') {
            var moneyToPay = (HowMuchForYourPoints($('input.PointsInput').val())).toFixed(2);
            //moneyToPay.substr(-3);
            //alert(moneyToPay);
            $('span.CostOfTransferMoney').text(' ' + moneyToPay + ' USD');
            $('span.MoneyToPayFinal').text(' ' + moneyToPay + ' USD');
            $('.PointsToDeclareFinal').text($('input.PointsInput').val());
            $('.centeredContent.myflights.PointsSteps span.second_header.CostOfTransfer').css('display', 'block');
            $('.centeredContent.myflights.PointsSteps .greySquare.step2  .loginBanner_errorText').css('visibility', 'hidden');
            //$(".greySquare.step2 .continueToTheNextLevel").bind("click");
        }
        else {
            $('.centeredContent.myflights.PointsSteps .greySquare.step2  .loginBanner_errorText').css('visibility', 'visible');
            $('.centeredContent.myflights.PointsSteps .loginBanner_errorText').text('Please Enter number of Points To calculate');

            $('span.CostOfTransferMoney').text('0');
            $('span.MoneyToPayFinal').text('0');
            $('.PointsToDeclareFinal').text('0');
            //$(".greySquare.step2 .continueToTheNextLevel").unbind("click");
        }
    });

    var element1 = $("div").find(".greySquare.step1");
    var element2 = $("div").find(".greySquare.step2");
    var element3 = $("div").find(".greySquare.step3");
    var element4 = $("div").find(".greySquare.step4");
    var element5 = $("div").find(".greySquare.step5");

    ///////////////////////  continueToTheNextLevel  missedFlights  /////////////////

    $("div.greySquare.step1 .continueToTheNextLevel span").click(function () {
        $(element1).css('display', 'none');
        $(element2).css('display', 'block');

        $('.continueToTheNextLevel span').css('width', '106px');

        $('.contactPop_steps ul li:nth-child(1)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(2)').addClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(3)').removeClass("checkinProcessActive");


        //preventing from click to next stage Untill entering numbers to calulate
        // $(".greySquare.step2 .continueToTheNextLevel").unbind("click");
        // $(".greySquare.step2 .continueToTheNextLevel").bind("click");


    });

    $("div.greySquare.step2 .continueToTheNextLevel span").click(function () {
        $(element2).css('display', 'none');
        $(element3).css('display', 'block');

        $('.continueToTheNextLevel').css('width', '196px');

        $('.contactPop_steps ul li:nth-child(1)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(2)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(3)').addClass("checkinProcessActive");
    });

    $("div.greySquare.step3 .continueToTheNextLevel span").click(function () {
        $(element3).css('display', 'none');
        $(element4).css('display', 'block');

        $('.continueToTheNextLevel').css('width', '196px');

        $('.contactPop_steps ul li:nth-child(1)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(2)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(3)').removeClass("checkinProcessActive");
    });


    $("div.greySquare.step4 .continueToTheNextLevel span").click(function () {
        $(element4).css('display', 'none');
        $(element5).css('display', 'block');

        $('.continueToTheNextLevel').css('width', '196px');

        $('.contactPop_steps ul li:nth-child(1)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(2)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(3)').removeClass("checkinProcessActive");
    });

    $("div.greySquare.step5 .continueToTheNextLevel span").click(function () {
        $(element5).css('display', 'none');
        $(element1).css('display', 'block');

        $('.continueToTheNextLevel').css('width', '106px');

        $('.contactPop_steps ul li:nth-child(1)').addClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(2)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(3)').removeClass("checkinProcessActive");
    });



    ///////////////////////  go To ThePreviusLevel  missedFlights /////////////////

    $("div.greySquare.step2 .goToThePreviusLevel").click(function () {
        $(element2).css('display', 'none');
        $(element1).css('display', 'block');

        $('.continueToTheNextLevel').css('width', '106px');

        $('.contactPop_steps ul li:nth-child(1)').addClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(2)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(3)').removeClass("checkinProcessActive");
    });

    $("div.greySquare.step3 .goToThePreviusLevel").click(function () {
        $(element3).css('display', 'none');
        $(element2).css('display', 'block');

        $('.continueToTheNextLevel').css('width', '106px');

        $('.contactPop_steps ul li:nth-child(1)').removeClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(2)').addClass("checkinProcessActive");
        $('.contactPop_steps ul li:nth-child(3)').removeClass("checkinProcessActive");
    });

});





function isBackEnable() {
    $('.myflights.missedFlights li.isBack').css('display', 'block');
}

function isBackDisable() {
    $('.myflights.missedFlights li.isBack').css('display', 'none');
}


var airWaysName = "";


function ChoosAirwys(element) {
    if ($(element.selectedOptions).text() == 'אל על') {
        $('.myflights.missedFlights .activityTable_form.ChoosAirwys_isElAl').css("display","inline-block");
        $('.myflights.missedFlights .activityTable_form.ChoosAirwys_isNotElAl').hide();

        $('.myflights.missedFlights .activityTable_form li.cardType').show();
    }
    else {
        $('.myflights.missedFlights .activityTable_form.ChoosAirwys_isElAl').hide();
        $('.myflights.missedFlights .activityTable_form.ChoosAirwys_isNotElAl').css("display", "inline-block");

        $('.myflights.missedFlights .activityTable_form li.cardType').hide();
    }
}

///////////////////////  PointsSteps Calculate  ////////////////////////




//$(document).ready(function () {
//    $("a.clickToCalculate").click(function () {
//        //alert();
//        $('.centeredContent.myflights.PointsSteps span.second_header.CostOfTransfer').css('display', 'block');
//    }

//    //if (getViewport_width() > 800) {
//    //    $('.myflights .activityTable_list li.activityTable_opened span.activityTable_openedDetails').removeClass('myflights_activityTable_list_mobile_span');
//    //    $('.activityTable_list li.activityTable_opened span.activityTable_openedDetails P').removeClass('activityTable_list_P_mobile_openedDetails_P');
//    //}
//    //else {
//    //    $('.myflights .activityTable_list li.activityTable_opened span.activityTable_openedDetails').addClass('myflights_activityTable_list_mobile_span');
//    //    $('.activityTable_list li.activityTable_opened span.activityTable_openedDetails P').addClass('activityTable_list_P_mobile_openedDetails_P');
//    //}
//});












/////////////////snowFlakes function in the interactive page -  page3 //////////////


var SCREEN_WIDTH = window.innerWidth / 2;
var SCREEN_HEIGHT = '800';
var container;
var particle;
var camera;
var scene;
var renderer;
var mouseX = 0;
var mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var particles = [];

var particleImage = new Image();//THREE.ImageUtils.loadTexture( "imgs/ParticleSmoke.png" );
particleImage.src = 'imgs/ParticleSmoke.png';


function init() {







    //container = document.createElement('div');
    //container = document.getElementById("canvasSnow");
    // var container = document.getElementById("canvasSnow");
    // var renderer = new THREE.WebGLRenderer();
    // renderer.setSize(container.offsetWidth, container.offsetHeight);
    //container.appendChild(renderer.domElement);
    // document.getElementById("canvasSnow").appendChild(renderer.domElement);
    // document.body.appendChild(container);

    container = document.getElementById("canvasSnow");

    camera = new THREE.PerspectiveCamera(75, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 10000);
    camera.position.z = 1000;
    scene = new THREE.Scene();
    scene.add(camera);

    renderer = new THREE.CanvasRenderer();
    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    var material = new THREE.ParticleBasicMaterial({ map: new THREE.Texture(particleImage) });

    for (var i = 0; i < 500; i++) {
        particle = new Particle3D(material);
        particle.position.x = Math.random() * 2000 - 1000;
        particle.position.y = Math.random() * 2000 - 1000;
        particle.position.z = Math.random() * 2000 - 1000;
        particle.scale.x = particle.scale.y = 1;
        scene.add(particle);

        particles.push(particle);
    }

    if (window.innerWidth > 980) {
        container.appendChild(renderer.domElement);

        // document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('touchstart', onDocumentTouchStart, false);
        document.addEventListener('touchmove', onDocumentTouchMove, false);
        setInterval(loop, 2500 / 60);

        // $('canvas').css('position', 'relative').css('top', '-950px').css('height', '946px');
    }
    else {
        // setInterval(loop, 8500 / 60);
    }



}

//function onDocumentMouseMove(event) {
//    mouseX = event.clientX - windowHalfX;
//    mouseY = event.clientY - windowHalfY;
//}
function onDocumentTouchStart(event) {
    if (event.touches.length == 1) {
        event.preventDefault();
        mouseX = event.touches[0].pageX - windowHalfX;
        mouseY = event.touches[0].pageY - windowHalfY;
    }
}

function onDocumentTouchMove(event) {
    if (event.touches.length == 1) {
        event.preventDefault();
        mouseX = event.touches[0].pageX - windowHalfX;
        mouseY = event.touches[0].pageY - windowHalfY;
    }
}

//
function loop() {
    for (var i = 0; i < particles.length; i++) {
        var particle = particles[i];
        particle.updatePhysics();

        with (particle.position) {
            if (y < -1000) y += 2000;
            if (x > 1000) x -= 2000;
            else if (x < -1000) x += 2000;
            if (z > 1000) z -= 2000;
            else if (z < -1000) z += 2000;
        }
    }

    camera.position.x += (mouseX - camera.position.x) * 0.05;
    camera.position.y += (-mouseY - camera.position.y) * 0.05;
    camera.lookAt(scene.position);
    renderer.render(scene, camera);
}






/////////////missed flights functions////////////////////////////////

function SearchButtonDates() {
    $('.homeMatmid_rightFluid.myflights.missedFlights .activityTable_form').hide()
    $('.homeMatmid_rightFluid.myflights.missedFlights .activityTable_form.FinalForm').show()
}


function MyFlightsBackToMain() {
    $('.homeMatmid_rightFluid.myflights.missedFlights .activityTable_form').show();
    $('.homeMatmid_rightFluid.myflights.missedFlights .activityTable_form.FinalForm').hide();
    $('.myflights.missedFlights .activityTable_form.ChoosAirwys_isNotElAl').hide();
    $(".activityTable_form.AddFlight input[type=text], textarea, input[type=select]").val("");

}




$('.innerContent.grey-upgrade .homeMatmid_engine_title').click(function (e) {

    e.preventDefault();
    $('.innerContent.grey-upgrade .homeMatmid_engine_title').removeClass('active');
    $(this).toggleClass('active');

    //alert($(this)) 
    if (($('.homeMatmid_engine_title.flight').hasClass('active'))) {
        $('.plazmaTab_formFrame.hotel').hide();
        $('.plazmaTab_formFrame.flight').show();
        $('.innerContent.grey-upgrade .homeMatmid_engine_title.active.hotel strong').css('color', 'darkblue');

        //$('.innerContent.grey-upgrade .homeMatmid_engine_title.active.hotel strong img').attr('src', '../imgs/_hotel_reservation-active.png');
       
    }

    else if (($('.homeMatmid_engine_title.hotel').hasClass('active'))) {
        $('.plazmaTab_formFrame.hotel').show();
        $('.plazmaTab_formFrame.flight').hide();
    }
});



$('.innerContent.grey-upgrade .homeMatmid_top a.homeMatmid_topFrame .homeMatmid_topBalance').click(function (e) {
    e.preventDefault();
    // alert($(this))
    if ($('.innerContent.grey-upgrade .homeMatmid_top.ScoreBoard').hasClass('active')) {
        $('.innerContent.grey-upgrade .homeMatmid_top.ScoreBoard').removeClass('active');
    }
    else {
        $('.innerContent.grey-upgrade .homeMatmid_top.ScoreBoard').addClass('active');
    }
});







////////////////////////////////////////////////// New Grey Home Page  scripts ///////////////////////////////////////////////////////////////////

//$("li.plazmaTab_formFrame_fligh.dateDetails.hide,.innerContent.grey-upgrade .plazmaTab_formFrame ul.plazmaTab_formFrame_primo input.ui-autocomplete-input,li.dateDetails-Li,li.plazmaTab_formFrame_primoDestination.plazmaTab_formFrame_error.hide,.plazmaTab_formFrame_btns.hide").css("display", "block ");

function colleps_greyEngine() {

    if ($(".innerContent.grey-upgrade .homeMatmid_left .plazmaTab_formFrame_primo_smallCombos ul input.ui-autocomplete-input").hasClass("active")) {

        $//("li.plazmaTab_formFrame_fligh.dateDetails.hide,.innerContent.grey-upgrade .plazmaTab_formFrame ul.plazmaTab_formFrame_primo input.ui-autocomplete-input,li.dateDetails-Li,li.plazmaTab_formFrame_primoDestination.plazmaTab_formFrame_error.hide,.plazmaTab_formFrame_btns.hide,.plazmaTab_formFrame_primo_passengers").css("display", "none");
        //$("li.plazmaTab_formFrame_fligh.dateDetails.hide,.innerContent.grey-upgrade .plazmaTab_formFrame ul.plazmaTab_formFrame_primo input.ui-autocomplete-input,li.dateDetails-Li,li.plazmaTab_formFrame_primoDestination.plazmaTab_formFrame_error.hide,.plazmaTab_formFrame_btns.hide,.plazmaTab_formFrame_primo_passengers.hide").css("z-index", "9999");
        //$(".innerContent.grey-upgrade .homeMatmid_left .homeMatmid_engine").css("min-height", "181px");
        //$(".plazmaTab_formFrame_primo_passengers.hide").css("height", "auto").css("width", "auto").css("position", "relative").css("top", "auto").css("display", "block");

        //$(".innerContent.grey-upgrade .homeMatmid_left .plazmaTab_formFrame_primo_smallCombos ul input.ui-autocomplete-input").removeClass("active");
        // $(".innerContent.grey-upgrade .homeMatmid_left .plazmaTab_formFrame_error input[type='text']").css("color", "#00003b !important");
    } 
    else {
        //$("li.plazmaTab_formFrame_fligh.dateDetails.hide,.innerContent.grey-upgrade .plazmaTab_formFrame ul.plazmaTab_formFrame_primo input.ui-autocomplete-input,li.dateDetails-Li,li.plazmaTab_formFrame_primoDestination.plazmaTab_formFrame_error.hide,.plazmaTab_formFrame_btns.hide,.plazmaTab_formFrame_primo_passengers.hide").css("display", "block ");
        $("li.plazmaTab_formFrame_fligh.dateDetails.hide,.innerContent.grey-upgrade .plazmaTab_formFrame ul.plazmaTab_formFrame_primo input.ui-autocomplete-input,li.dateDetails-Li,li.plazmaTab_formFrame_primoDestination.plazmaTab_formFrame_error.hide,.plazmaTab_formFrame_btns.hide,.plazmaTab_formFrame_primo_passengers").css("display", "block");
        $(".innerContent.grey-upgrade .homeMatmid_left .homeMatmid_engine").css("min-height", "462px");
       // $(".plazmaTab_formFrame_primo_passengers.hide").css("height", "auto").css("width", "auto").css("position", "relative").css("top", "auto").css("display", "block");

        $(".innerContent.grey-upgrade .homeMatmid_left .plazmaTab_formFrame_primo_smallCombos ul input.ui-autocomplete-input").addClass("active");
        // $(".innerContent.grey-upgrade .homeMatmid_left .plazmaTab_formFrame_error input[type='text']").css("color", "lightgrey !important");
    }
}


/// change height in the top page click for points info
$(document).ready(function () {

    if (innerWidth < 550) {

        $(".innerContent.grey-upgrade .plazmaTab_formFrame.flight ul.plazmaTab_formFrame_primo li,.innerContent.grey-upgrade .plazmaTab_formFrame ul.plazmaTab_formFrame_primo input.ui-autocomplete-input,li.dateDetails-Li,li.plazmaTab_formFrame_primoDestination.plazmaTab_formFrame_error.hide,.plazmaTab_formFrame_btns.hide,.plazmaTab_formFrame_primo_passengers.hide").css("display", "none");
        $(".innerContent.grey-upgrade .homeMatmid_left .plazmaTab_formFrame_primo_smallCombos ul span.plazmaTab_formFrame_primo_smallComboHolder").click(function () {
            colleps_greyEngine();
        });

        //////// open close homeMatmid_services.Entitlements /////

        $(".innerContent.grey-upgrade .homeMatmid_services .homeMatmid_services_title").addClass("active");
        $('.innerContent.grey-upgrade .homeMatmid_services .homeMatmid_services_title').click(function (e) {
            e.preventDefault();
            //alert(this);
            $(this).toggleClass('active');
        });

    }
});