﻿var mfeLang = mfeLang || "he";

; (function ($) {
    'use strict'
    var tmpl = function (str, data) {
        var f = !/[^\w\-\.:]/.test(str)
          ? tmpl.cache[str] = tmpl.cache[str] || tmpl(tmpl.load(str))
          : new Function(// eslint-disable-line no-new-func
            tmpl.arg + ',tmpl',
            'var _e=tmpl.encode' + tmpl.helper + ",_s='" +
              str.replace(tmpl.regexp, tmpl.func) + "';return _s;"
          )
        return data ? f(data, tmpl) : function (data) {
            return f(data, tmpl)
        }
    }
    tmpl.cache = {}
    tmpl.load = function (id) {
        return document.getElementById(id).innerHTML
    }
    tmpl.regexp = /([\s'\\])(?!(?:[^{]|\{(?!%))*%\})|(?:\{%(=|#)([\s\S]+?)%\})|(\{%)|(%\})/g
    tmpl.func = function (s, p1, p2, p3, p4, p5) {
        if (p1) { // whitespace, quote and backspace in HTML context
            return {
                '\n': '\\n',
                '\r': '\\r',
                '\t': '\\t',
                ' ': ' '
            }[p1] || '\\' + p1
        }
        if (p2) { // interpolation: {%=prop%}, or unescaped: {%#prop%}
            if (p2 === '=') {
                return "'+_e(" + p3 + ")+'"
            }
            return "'+(" + p3 + "==null?'':" + p3 + ")+'"
        }
        if (p4) { // evaluation start tag: {%
            return "';"
        }
        if (p5) { // evaluation end tag: %}
            return "_s+='"
        }
    }
    tmpl.encReg = /[<>&"'\x00]/g // eslint-disable-line no-control-regex
    tmpl.encMap = {
        '<': '&lt;',
        '>': '&gt;',
        '&': '&amp;',
        '"': '&quot;',
        "'": '&#39;'
    }
    tmpl.encode = function (s) {
        return (s == null ? '' : '' + s).replace(
          tmpl.encReg,
          function (c) {
              return tmpl.encMap[c] || ''
          }
        )
    }
    tmpl.arg = 'o'
    tmpl.helper = ",print=function(s,e){_s+=e?(s==null?'':s):_e(s);}" +
                    ',include=function(s,d){_s+=tmpl(s,d);}'
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return tmpl
        })
    } else if (typeof module === 'object' && module.exports) {
        module.exports = tmpl
    } else {
        $.tmpl = tmpl
    }
}(this))


var mfe = {};
(function ($, mfe) {
    $(function () {

        function errorDemo(elmQuery) {
            //  e.preventDefault();
            //  e.stopPropagation();
            $(elmQuery).parents(".form-group").toggleClass("has-error");
            return false;
        }

        function openPart(objData) {

            var d = new Date();
            var n = d.getMilliseconds();
            var partQuery = objData.partQuery,
                partUrl = objData.partUrl,
                partJson = objData.partJson || partUrl;

            var helperDiv = $('<script type="text/x-tmpl" id="tmpl-demo-' + n + '"></script>');
            $("body").append(helperDiv);
            helperDiv.load("templates/" + partUrl + ".html?v=" + n, function () {
                var jsonUrl = "dictionary/" + partJson + "-he.js?v=" + n;

                $.getJSON(jsonUrl, function (data) {
                    function generator(data) {

                        $(partQuery).html(tmpl('tmpl-demo-' + n, data));
                       
                        helperDiv.remove();
                     
                            if (objData.callback) {
                                mfe[objData.callback]();
                            };

                    
                     
                        if (objData.child) {
                            for (var i = 0; i < objData.child.length; i++) {
                                openPart(objData.child[i]);
                            }
                        }
                        //hack
                        var side = (mfeLang !="he")?"right":"left";
                        $('[data-toggle="popover"]').popover({
                            // Add more else stuff with parentheses for more sizes
                            placement: function () { return $(window).width() < 767 ? 'bottom' : side; }
                        });
                    }

                    //   var jsonUrl = "dictionary/" + partUrl + "-" + mfeLang + ".json?v=" + n;
                    if (mfeLang == "he") {
                        generator(data);

                    } else {

                        var jsonUrl2 = "dictionary/" + partJson + "-" + mfeLang + ".js?v=" + n;
                        $.getJSON(jsonUrl2, function (data2) {
                            var mixData = $.extend(true, data, data2);
                            generator(mixData);
                        }).fail(function () {
                            generator(data);
                        });

                    }
                    //   data  $.extend(true, object1, object2);


                });

            });

        }

        function loadImagesHelper() {
            var helperDiv = $('<div class="mfe-developer-helper js-mfe-developer-helper  navbar-fixed-bottom pull-right hidden-xs" style="width:200px; bottom:0; overflow:auto;max-height:300px; position:fixed;opacity:0.8"></div>');
            $("body").append(helperDiv);

            openPart({ "partQuery": ".js-mfe-developer-helper", "partUrl": "helper/helper-links" });


            //    openPart({ "partQuery": ".js-mfe-developer-helper", "partUrl": "helper/helper-links" });
            helperDiv.on("click", ".list-group-item a", function () {

                $("#mfe_main_image").attr("src", $(this).data("image"));
            });
        }
        function loadHelper() {
            var helperDiv = $('<div class="mfe-developer-helper js-mfe-developer-helper  navbar-fixed-bottom pull-right hidden-xs" style="width:200px; bottom:0;overflow:auto;max-height:300px; position:fixed;opacity:0.8"></div>');
            $("body").append(helperDiv);

            openPart({ "partQuery": ".js-mfe-developer-helper", "partUrl": "helper/helper-links-pages", "partJson": "helper/helper-links" });
          openPart({ "partQuery": ".js-mfe-main-part", "partUrl": "otp/page-home-otp", "child": [{ "partQuery": ".js-mfe-home-banners", "partUrl": "otp/part-home-boxes" }, { "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1" }, { "partQuery": ".js-mfe-footer-links", "partUrl": "otp/home-opt-footer-links","callback":"openHashLink" }] });

            //    openPart({ "partQuery": ".js-mfe-developer-helper", "partUrl": "helper/helper-links" });
            helperDiv.on("click", "a", function (e) {

                var $self = $(this);
                var dataAttr = $self.data("obj");
                if (dataAttr) {
                  //  e.preventDefault();
                    //e.stopPropagation();
                    var objStr = JSON.stringify($self.data("obj"));

                    var objData = JSON.parse(objStr);


                    openPart(objData);

                  //  return false;
                }
            });

            $('body').on("click", ".js-mfe-card-flip", function (e) {

                var $self = $(this);

                openPart({ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" });
                $self.parents(".mfe-card").toggleClass("mfe-flip");


            });
        }

        //  function loadoldHelper() {
        //      var d = new Date();
        //      var n = d.getMilliseconds();
        //      var helperDiv = $('<div class="mfe-developer-helper  navbar-fixed-bottom pull-right hidden-xs" style="width:200px; bottom:0; position:fixed;opacity:0.8"></div>');

        //      helperDiv.load("templates/helper-links.html", function () {
        //          $("body").append(helperDiv);
        //          openPart({ "partQuery": ".js-mfe-main-part", "partUrl": "otp/page-home-otp", "child": [{ "partQuery": ".js-mfe-home-banners", "partUrl": "otp/part-home-boxes" }, { "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1" }, { "partQuery": ".js-mfe-footer-links", "partUrl": "otp/home-opt-footer-links" }] });
        //      //    openPart({ "partQuery": ".js-mfe-home-banners", "partUrl": "otp/part-home-boxes" });

        //          helperDiv.on("click", "a", function (e) {

        //              var $self = $(this);
        //              var dataAttr = $self.data("obj");
        //              if (dataAttr){
        //e.preventDefault();
        //              e.stopPropagation();
        //              var objStr = JSON.stringify($self.data("obj"));

        //              var objData = JSON.parse(objStr);


        //              openPart(objData);
        //              return false;}
        //          })
        //      });


        //      $('body').on("click", ".js-mfe-card-flip", function (e) {

        //          var $self = $(this);

        //          openPart({ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" });
        //          $self.parents(".mfe-card").toggleClass("mfe-flip");


        //      });
        //  }
        if ($("body").hasClass("js-images-demo")) {
            loadImagesHelper();

        } else {


            loadHelper();

        }

        function errorDemo(elmQuery) {
            //  e.preventDefault();
            //  e.stopPropagation();
            $(elmQuery).parents(".form-group").toggleClass("has-error");
            return false;
        }

        mfe.showPassword = function () {

            errorDemo("#member_number");
        };
    mfe.showTab2 = function () {

        $('#tab2link').tab('show');

    };
     mfe.showTab2Error = function () {

        $('#tab2link').tab('show');
 $('#error1').toggleClass("hidden");
    };

     mfe.errorCard = function () {
$(".mfe-card-flip-box").toggleClass("mfe-flip");
  
    };

mfe.openHashLink= function () {
var hashObj = $(location.hash+"_link").data("obj");
if(!hashObj){
var hashObj = $("#page_1_link").data("obj");
}

                     var objStr = JSON.stringify(hashObj);
                    var objData = JSON.parse(objStr);
                    openPart(objData);
};
    });
})(jQuery, mfe);