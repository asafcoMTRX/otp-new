﻿function flightsGallery(galOverflow, galList, galItem, galLeft, galRight) {
    if (isIE8) {
        $(galOverflow).addClass(galOverflow.split('.')[1] + "_ie8");
        $(galList).addClass(galList.split('.')[1] + "_ie8");
    }

    this.galleryOverflow = $(galOverflow);
    this.galleryList = $(galList);
    this.galleryItem = $(galItem);
    this.galleryLeft = $(galLeft);
    this.galleryRight = $(galRight);
    this.galleryItem_length = this.galleryItem.length;
    this.flightsPlace = 0;

    this.refresh_flightsGallery = function () {
        this.galleryWidth = this.galleryOverflow.width();
        this.galleryItem.width(this.galleryWidth);
        this.galleryList.width(this.galleryItem_length * this.galleryWidth);
        this.galleryList.css('right', (-1 * (this.flightsPlace * this.galleryWidth)));
    };

    this.flightsGallery_move = function (bin) {
        if (bin) {
            if (self.flightsPlace < self.galleryItem_length - 1) {
                if (isIE8) {
                    self.flightsPlace++;
                    newRight = -1 * (self.flightsPlace * self.galleryWidth);
                    self.galleryList.animate({ right: newRight }, 500);
                }
                else {
                    self.flightsPlace++;
                    self.galleryList.css('right', (-1 * (self.flightsPlace * self.galleryWidth)));
                }
            }
        }
        else {
            if (self.flightsPlace > 0) {
                if (isIE8) {
                    self.flightsPlace--;
                    newRight = -1 * (self.flightsPlace * self.galleryWidth);
                    self.galleryList.animate({ right: newRight }, 500);
                }
                else {
                    self.flightsPlace--;
                    self.galleryList.css('right', (-1 * (self.flightsPlace * self.galleryWidth)));
                }
            }
        }

        if (self.flightsPlace == 0) {
            self.galleryRight.addClass('homeMatmid_flights_disabled');
            self.galleryLeft.removeClass('homeMatmid_flights_disabled');
        }
        else if (self.flightsPlace == self.galleryItem_length - 1) {
            self.galleryRight.removeClass('homeMatmid_flights_disabled');
            self.galleryLeft.addClass('homeMatmid_flights_disabled');
        }
        else {
            self.galleryRight.removeClass('homeMatmid_flights_disabled');
            self.galleryLeft.removeClass('homeMatmid_flights_disabled');
        }
    };

    if (isIE8) {
        var self = this;
        this.galleryLeft.on('click', function (event) { self.flightsGallery_move(true); return false; });
        this.galleryRight.on('click', function (event) { self.flightsGallery_move(false); return false; });
    }
    else {
        self = this;
        this.galleryLeft.click(function (e) { self.flightsGallery_move(true); return false; });
        this.galleryRight.click(function (e) { self.flightsGallery_move(false); return false; });
    }

    this.refresh_flightsGallery();
}