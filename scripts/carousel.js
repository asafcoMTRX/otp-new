var isIE8 = false;
if (navigator.userAgent.indexOf("MSIE 8") > -1) {
        isIE8 = true;
}

function setupSlider(sliderObj, slideList, slideItem, slideLeft, slideRight)
{
	if(isIE8)
	{
		$(slideList).addClass(slideList.split('.')[1]+"_ie8");
	}

	this.sliderObject = sliderObj;
	this.sliderList = slideList;
	this.sliderItem = slideItem;
	this.sliderLeft = slideLeft;
	this.sliderRight = slideRight;
	this.refreshing = false;
	this.sliderTimeout = 0;
	this.listPosition = 0;
	this.leftMost = 0;
	this.rightMost = 0;
	this.atEnd = [true,true];

	this.refreshSlider = function(){
		if(!this.refreshing)
		{
			this.refreshing = true;
			this.listWidth = $(this.sliderList).width();
			this.overflowWidth = $(this.sliderObject).width();

			if(this.listWidth>this.overflowWidth)
			{
				$(this.sliderList).css('margin','0px 0px');
				if(getViewport_width()>670)
				{
					$(this.sliderLeft).css('display','block');
					$(this.sliderRight).css('display','block');
					if(($('body').css('direction')=="rtl")&&(!isIE8)) $(this.sliderRight).css('display','none');
					else if(($('body').css('direction')=="ltr")&&(!isIE8)) $(this.sliderLeft).css('display','none');
				}
				else
				{
					$(this.sliderLeft).css('display','');
					$(this.sliderRight).css('display','');
				}

				if($('body').css('direction')=="rtl") this.listPosition = parseInt(-1*(this.listWidth-this.overflowWidth));
				else if($('body').css('direction')=="ltr") this.listPosition = 0;
			}
			else
			{
				this.listPosition = 0;
				$(this.sliderLeft).css('display','none');
				$(this.sliderRight).css('display','none');
				$(this.sliderList).css('margin','0px auto');
			}

			this.leftMost = 0;
			this.rightMost = -1*(this.listWidth-this.overflowWidth);
			this.refreshing = false;
		}
	}	
	
	this.slide = function (bin){
		if(!isIE8)
		{
			if(parseInt($(this.sliderList).css('transform').split('(')[1].split(')')[0].split(',')[12]))
			xPosition = parseInt($(this.sliderList).css('transform').split('(')[1].split(')')[0].split(',')[12]);
			else xPosition = parseInt($(this.sliderList).css('transform').split('(')[1].split(')')[0].split(',')[4]);
	
			if((xPosition>=this.rightMost)&&(xPosition<=this.leftMost))
			{
				if(bin&&this.atEnd[0])
				{
					this.myScroll.scrollBy(-10, 0);
					this.atEnd = [true,true];

					if(getViewport_width()>670)
					{
						$(this.sliderLeft).css('display','block');
						$(this.sliderRight).css('display','block');
					}
				}
				else if(!bin&&this.atEnd[1])
				{
					this.myScroll.scrollBy(10, 0);
					this.atEnd = [true,true];

					if(getViewport_width()>670)
					{
						$(this.sliderLeft).css('display','block');
						$(this.sliderRight).css('display','block');
					}
				}
				var self = this;
				if (navigator.appName == 'Microsoft Internet Explorer') this.sliderTimeout = setTimeout(function(){self.slide(bin);}, 30);
				else this.sliderTimeout = setTimeout(function(){self.slide(bin);}, 10);
			}
			else if(xPosition<this.rightMost)
			{
				this.myScroll.scrollTo(this.rightMost, 0);
				this.atEnd = [false,true];

				if(getViewport_width()>670)
				{
					$(this.sliderLeft).css('display','block');
					$(this.sliderRight).css('display','none');
				}
				
			}
			else if(xPosition>this.leftMost)
			{
				this.myScroll.scrollTo(this.leftMost, 0);
				this.atEnd = [true,false];

				if(getViewport_width()>670)
				{
					$(this.sliderLeft).css('display','none');
					$(this.sliderRight).css('display','block');
				}
			}			
		}
		else
		{
			var xPosition = parseInt($(this.sliderList).css('left'));
			if((xPosition>=this.rightMost)&&(xPosition<=this.leftMost))
			{
				var self = this;
				if(bin&&this.atEnd[0])
				{
					$(this.sliderList).css('left', xPosition-=10);
					this.atEnd = [true,true];
				}
				else if(!bin&&this.atEnd[1])
				{
					$(this.sliderList).css('left', xPosition+=10);
					this.atEnd = [true,true];
				}

				if(getViewport_width()>670)
				{
					$(this.sliderLeft).css('display','block');
					$(this.sliderRight).css('display','block');
				}

				this.sliderTimeout = setTimeout(function(){
					self.slide(bin);
				}, 2);
			}
			else if(xPosition<this.rightMost)
			{
				$(this.sliderList).css('left', this.rightMost);
				this.atEnd = [false,true];
			}
			else if(xPosition>this.leftMost)
			{
				$(this.sliderList).css('left', this.leftMost);
				this.atEnd = [true,false];
			}
		}
	}
	
	this.stopSlider = function(){
		clearTimeout(this.sliderTimeout);
	}

	this.refreshSlider();
	if(!isIE8)
	{
		this.myScroll = new IScroll( this.sliderObject, { eventPassthrough: true, scrollX: true, scrollY: false, startX:this.listPosition, preventDefault: false });
//		placeTo_show = parseInt($(this.sliderItem).length/2);
//		this.myScroll.scrollToElement($(this.sliderItem)[placeTo_show], 0, true);
	}
	else
	{
		$(this.sliderObject).css('height', $(this.sliderList).height());
		$(this.sliderList).css('left', this.listPosition);
	}

	$(this.sliderLeft).bind('touchstart', function(e){this.slide(false); e.preventDefault();});
	$(this.sliderRight).bind('touchstart', function(e){this.slide(true); e.preventDefault();});
	$(this.sliderLeft).bind('touchend', function(e){this.stopSlider(); e.preventDefault();});
	$(this.sliderRight).bind('touchend', function(e){this.stopSlider(); e.preventDefault();});

	selfObj = this;
	$(this.sliderLeft).mouseover(function(e) {selfObj.slide(false); e.preventDefault();})
		.mouseout(function(e) {selfObj.stopSlider(); e.preventDefault();});

	$(this.sliderRight).mouseover(function(e) {selfObj.slide(true); e.preventDefault();})
		.mouseout(function(e) {selfObj.stopSlider(); e.preventDefault();});

	$(this.sliderLeft).click(function(e) {return false; e.preventDefault();});
	$(this.sliderRight).click(function(e) {return false; e.preventDefault();});
}