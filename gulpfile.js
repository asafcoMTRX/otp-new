﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp'),
    sass = require("gulp-sass"),
    livereload = require('gulp-livereload');


var paths = {
    sass: "css/",
    outputDirectory: "css/"
};
gulp.task("sass:compile", function () {
    return gulp.src(paths.sass + "**/*.scss")
        .pipe(sass())
        .pipe(gulp.dest(paths.sass))
        .pipe(livereload());
});

gulp.task('watch', function () {
    livereload.listen();
    gulp.watch('scss/parts/*.scss', ['less']);
});

gulp.task('default', function () {
    // place code for your default task here
});