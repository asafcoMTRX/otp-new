﻿{
  "labels": {
    "more": "לפרטים נוספים >"
  },
  "items": [
    {
      "title": "הצטרף לנוסע המתמיד",
      "image": "imgs/otp/login_services01.png",
      "imageAlt": "תאור התמונה",
      "link": "http://elal.com"
    },
    { 
      "title": "הטבות חברי מועדון",
      "image": "imgs/otp/login_services02.png",
      "imageAlt": "תאור התמונה",
      "link": "http://elal.com"
    },
    {
      "title": "FLY CARD",
      "image": "imgs/otp/login_services03.png",
      "imageAlt": "תאור התמונה",
      "link": "http://elal.com"
    },
    {
      "title": "מימוש נקודות",
      "image": "imgs/otp/login_services04.png",
      "imageAlt": "תאור התמונה",
      "link": "http://elal.com"
    }
    ,
    {
      "title": "צבירת נקודות",
      "image": "imgs/otp/login_services05.png",
      "imageAlt": "תאור התמונה",
      "link": "http://elal.com"
    }
    ,
      {
      "title": "שותפים",
      "image": "imgs/otp/login_services06.png",
      "imageAlt": "תאור התמונה",
      "link": "http://elal.com"
    }
  ]

}
