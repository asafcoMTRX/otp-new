﻿{
  "labels": {
    "popoverSide": "right",
    "title": "Enter My Account",
    "ButtonEntrancePassword": "Login with permanent password",
    "ButtonEntranceSMS": "Enter the code in SMS or Email",
    "WrongeDetails": "Wronge Details",
    "memberId": "Member Number",
    "password": "Password",
    "forgotPassword": "Forgot Password",
    "forgotMemberId": "Forgot Member ID",
    "errorInType": "Error In Type",
    "nextButton": "Next",
    "getCodeButton": "Get Code",
    "emailButton": "Email",
    "cellPhoneButton": "CellPhone",
    "myAccountText1": "I would like to receive a verification code via:",
    "myAccountText2": "Enter your mobile number as defined entrance to the club:",
    "myAccountText3": "After filling will send you a one-time code to enter the personal area:",
    "phoneNumber": "Cell-Phone",
    "popupText": "Lorem Ipsum is simply dummy text of the printing and typesetting in   n printer took a galley of type and scrambled it to make a type specimen book. "
  }

}
