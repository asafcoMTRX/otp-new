﻿{
  "labels": {
    "popoverSide": "left",
    "title": "כניסה לחשבון שלי",
    "ButtonEntrancePassword": "כניסה עם סיסמא קבועה",
    "ButtonEntranceSMS": "כניסה עם קוד ב- SMS/דוא\"ל",
    "WrongeDetails": "פרטים לא נכונים",
    "memberId": "מספר חבר",
    "password": "סיסמא",
    "forgotPassword": "שכחתי סיסמא",
    "forgotMemberId": "שכחתי מספר חבר",
    "errorInType": "טעות בהקלדה",
    "nextButton": "המשך",
    "getCodeButton": "קבלת קוד",
    "emailButton": "דואל",
    "cellPhoneButton": "נייד",
    "myAccountText1": "ברצוני לקבל את קוד האימות באמצעות:",
    "myAccountText2": "הקלד את מספר הנייד שלך כפי שהוגדר בכניסה למועדון:",
    "myAccountText3": "לאחר מילוי הפרטים ישלח לך קוד לכניסה חד פעמית לאיזור האישי:",
    "phoneNumber": "מספר נייד",
    "popupText": "להאמית קרהש רם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולהע צופעט למרקוח איב דולור סיט אמט,"
  }
}
