﻿{
    "links":[
        {"name":"דף פתיחה ",
            "image":"demos/OTP_04_0000_enter_with_pass.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-main-part", "partUrl": "otp/page-home-otp", "child": [{ "partQuery": ".js-mfe-home-banners", "partUrl": "otp/part-home-boxes","partJson": "otp/part-home-boxes-2" }, { "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1" }, { "partQuery": ".js-mfe-footer-links", "partUrl": "otp/home-opt-footer-links" }] }
        },
        {"name":"דף פתיחה + 2 באנרים",
            "image":"demos/OTP_04_0001_2 Banners Exp.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-home-banners", "partUrl": "otp/part-home-boxes" }
        },
        {"name":"הצג סיסמה",
            "image":"demos/OTP_04_0002_enter_with_pass_with_Complete_fields.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1" }
        },
        {"name":"סיממה מוצגת",
        "image":"demos/OTP_04_0003_enter_with_pass_with_Complete_fields%20copy.jpg",
        "link":"",
        "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1" }
        },
        {"name":"כניסה מהירה",
            "image":"demos/OTP_04_0004_Member_entry_phone.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showTab2","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"כתובות מייל",
            "image":"demos/OTP_04_0005_Member_entry_email.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","partJson":"otp/card-home2","callback":"showTab2","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"קוד זיהוי",
            "image":"demos/OTP_04_0006_Member_entry.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","child":[{ "partQuery": ".js-mfe-card-tad2", "partUrl": "otp/card-home3","callback":"showTab2" }] }
        },
        {"name":"קוד זיהוי מלא",
            "image":"demos/OTP_04_0007_Member_entry_with_Complete_fields.jpg",
            "link":"",
           "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","child":[{ "partQuery": ".js-mfe-card-tad2", "partUrl": "otp/card-home3","partJson": "otp/card-home3-1","callback":"showTab2" }] }
          },
        {"name":"קוד זיהוי עם שגיאה",
            "image":"demos/OTP_04_0008_Member_entry_validation.jpg",
            "link":"",
           "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","child":[{ "partQuery": ".js-mfe-card-tad2", "partUrl": "otp/card-home3","partJson": "otp/card-home3-1","callback":"showTab2Error" }] }
          },
        {"name":"חלון שגיאה",
            "image":"demos/OTP_04_0009_popup_Member_entry.jpg",
            "link":"",
                      "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","child":[{ "partQuery": ".js-mfe-card-tad2", "partUrl": "otp/card-home3","partJson": "otp/card-home3-1","callback":"showTab2Error" },{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1","callback":"errorCard" }] }
        },
        {"name":"כניסה 2",
            "image":"demos/OTP_07_0000_enter_with_pass.jpg",
            "link":"",
              "obj":{ "partQuery": ".js-mfe-main-part", "partUrl": "otp/page-home-otp", "child": [{ "partQuery": ".js-mfe-home-banners", "partUrl": "otp/part-home-boxes","partJson": "otp/part-home-boxes-2" }, { "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","partJson": "otp/card-home1-2" }, { "partQuery": ".js-mfe-footer-links", "partUrl": "otp/home-opt-footer-links" }] }
       
        },
        {"name":"כניסה 2 + 2 באנרים",
            "image":"demos/OTP_07_0001_2 Banners Exp.jpg",
            "link":"",
                  "obj":{ "partQuery": ".js-mfe-home-banners", "partUrl": "otp/part-home-boxes"}
        },
        {"name":"שכחתי סיסמה",
            "image":"demos/OTP_07_0004_forgot_password.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home4","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"2 שכחתי סיסמה",
            "image":"demos/OTP_07_0005_forgot_password_mail.jpg",
            "link":"",
          "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home5","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0016 ",
            "image":"demos/OTP_07_0006_forgot_password_mail_not_valid.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0017 ",
            "image":"demos/OTP_07_0007_forgot_password_Questions.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0018 ",
            "image":"demos/OTP_07_0008_forgot_MemberID .jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0018 ",
            "image":"demos/OTP_07_0008_forgot_MemberID .jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0019 ",
            "image":"demos/OTP_07_0009_forgot_MemberID_Questions .jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0020 ",
            "image":"demos/OTP_07_0010_forgot_MemberID_Done .jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0021 ",
            "image":"demos/OTP_07_0011_fogot_MemberID_error.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0022 ",
            "image":"demos/OTP_07_0012_fogot_MemberID_error_2.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0023 ",
            "image":"demos/OTP_07_0013_New_password.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0024 ",
            "image":"demos/OTP_07_0014_fogot_password_error.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0025 ",
            "image":"demos/OTP_07_0015_fogot_password_error_2.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0026 ",
            "image":"demos/OTP_07_0016_Member_entry_phone.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0027 ",
            "image":"demos/OTP_07_0017_forgot_password_phone_not_valid.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0028 ",
            "image":"demos/OTP_07_0018_Member_entry_email.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0029 ",
            "image":"demos/OTP_07_0019_Member_entry.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0030 ",
            "image":"demos/OTP_07_0020_Member_entry_with_Complete_fields.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0031 ",
            "image":"demosOTP_07_0021_Member_entry_validation.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0032 ",
            "image":"demos/OTP_07_0022_popup_Member_entry.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0033 ",
            "image":"demos/OTP_07_0023_block_account.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0034 ",
            "image":"demos/OTP_08_0004_captcha_incorrect_password.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0035 ",
            "image":"demos/OTP_08_0005_forgot_password.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0036 ",
            "image":"demos/OTP_08_0025_block_account_2.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        },
        {"name":"דף 0036 ",
            "image":"demos/OTP_08_0011_forgot_Password_done +.jpg",
            "link":"",
            "obj":{ "partQuery": ".js-mfe-otp-home-card", "partUrl": "otp/card-home1","callback":"showPassword","child":[{ "partQuery": ".js-mfe-card-back", "partUrl": "otp/card-alert-1" }] }
        }

    ] 
}
